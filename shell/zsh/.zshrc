# tmux {{{1

[ -z "$TMUX" ] && { tmux attach || tmux new-session }

# shellrc {{{1

SHELL=zsh
source ~/.shellrc

# Configuration options {{{1

# Do not require a leading ‘.’ in a filename to be matched explicitly.
#
# Whenever you use a wildcard, there is no need to use the dot prefix in
# order to match those filenames that start with the dot character. They will
# be matched whether or not the dot character is specified at the beginning of
# the pattern that contains a wildcard.
setopt GLOBDOTS

# Do not query the user before executing ‘rm *’ or ‘rm path/*’.
setopt RM_STAR_SILENT

#  If this option is unset, output flow control via start/stop characters
#  (usually assigned to ^S/^Q) is disabled in the shell’s editor.
unsetopt FLOW_CONTROL

# If a command is issued that can’t be executed as a normal command, and the
# command is the name of a directory, perform the cd command to that directory.
# This option is only applicable if the option SHIN_STDIN is set, i.e. if
# commands are being read from standard input. The option is designed for
# interactive use; it is recommended that cd be used explicitly in scripts to
# avoid ambiguity.
setopt AUTO_CD

# Prompt {{{1

function zle-line-init zle-keymap-select {
		PROMPT_STYLE="%B"
		INPUT_STYLE="%b%F{white}"
    VIMMODE="%F{white}${${KEYMAP/vicmd/N}/main/I}"
		LAST_EXIT_CODE="%(?.%F{white}.%F{red})%(?..%? )"

		PROMPT="$PROMPT_STYLE$LAST_EXIT_CODE$VIMMODE $INPUT_STYLE"

    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

# Directory stack {{{1

# The maximum size of the directory stack, by default there is no limit. If the
# stack gets larger than this, it will be truncated automatically. This is
# useful with the AUTO_PUSHD option.
#
# If set to "0", then there is no limit.
DIRSTACKSIZE=8

# Make cd push the old directory onto the directory stack.
setopt AUTO_PUSHD

# +N (start from the left, when "dirs" has been executed, or top, when "dirs -v" has been executed)
# -N (start from the right, when "dirs" has been executed, or bottom, when 'dirs -v" has been executed)
# Remember that "cd -0" is different than "cd +0"

# History {{{1

setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY

SAVEHIST=50000
HISTSIZE=$SAVEHIST
HISTFILE=~/.zsh_history

# Word editing {{{1

WORDCHARS=''

# Completion {{{1

# Loading {{{2

# Load completion
autoload compinit
compinit

# Make the "menuselect" keymap to be available.
zmodload zsh/complist

# General {{{2

zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' format ' %F{yellow}-- %d --%f'
zstyle ':completion:*:warnings' format '%BNo matches found for: %d%b'

# Make flags from commands to be displayed without their corresponding
# descriptions when menu selection has been triggered for the flags (i.e.
# after the hyphen character.)
# zstyle ':completion:*:complete:*:*:options' verbose 

# standard tag: all {{{2

zstyle ':completion:*:complete:(kill|mv|cp):*:*' ignore-line yes

# standard tag: manual {{{2
# Commands
# 	man

zstyle ':completion:*:complete:*:*:manuals' menu yes

# standard tag: processes {{{2
# Commands
# 	kill
# 	wait

zstyle ':completion:*:complete:*:*:processes' command "ps -u $(whoami) -o pid,%cpu,args"
zstyle ':completion:*:complete:*:*:processes' menu yes
zstyle ':completion:*:complete:kill:*:processes' command "ps -u $(whoami) --forest"
zstyle ':completion:*:complete:kill:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"

# standard tag: directory-stack {{{2
# Commands
# 	cd -

zstyle ':completion:*:complete:cd:*:directory-stack' menu yes

# Keybindings {{{2

bindkey -M menuselect '^a' beginning-of-line
bindkey -M menuselect '^e' end-of-line
bindkey -M menuselect "^n" down-line-or-history
bindkey -M menuselect "^p" up-line-or-history
bindkey -M menuselect '^o' accept-and-infer-next-history
bindkey -M menuselect "^s" history-incremental-search-forward
bindkey -M menuselect '^[a' accept-and-hold

# Keybindings {{{1

autoload -U edit-command-line
zle -N edit-command-line

function OTFls() {
	echo
	mls
	zle redisplay
}
zle -N OTFls

function OTFpwd() {
	echo
	pwd
	zle redisplay
}
zle -N OTFpwd

function vibindkey() {
  bindkey -M viins "$@"
  bindkey -M vicmd "$@"
}

# Mappings

bindkey -v

# Miscelanous

bindkey "^i" complete-word
bindkey "^w" backward-kill-word
bindkey "^h" backward-delete-char
bindkey "^d" delete-char-or-list
bindkey "^a" beginning-of-line
bindkey "^e" end-of-line
bindkey "^f" forward-char
bindkey "^b" backward-char

vibindkey '^x^e' edit-command-line
vibindkey '^q' push-line
vibindkey '^[H' run-help
vibindkey '^[J' OTFls
vibindkey '^[K' OTFpwd

bindkey -M vicmd "\`" vi-goto-mark
bindkey -M vicmd "'" vi-goto-mark-line

bindkey -M viins "^u" kill-whole-line
bindkey -M viins "^k" kill-line
bindkey -M viins "^[b" backward-word
bindkey -M viins "^[f" forward-word
bindkey -M viins "^[p" history-search-backward
bindkey -M viins "^[n" history-search-forward

# Disabled

bindkey -r "^?"
bindkey -r "^M"

# Plugins {{{1

# N: zsh-autosuggestions
# R: github.com/zsh-users/zsh-autosuggestions

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=247"

# N: fzf

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Experimental {{{1

# Given two functions that send replies, only use those replies that are sent
# by the function with higher priority.

function f1() { reply=(a b c) }
function f2() { reply=(1 2 3) }

compctl -K f1 + -K f2 a2

# Given an arbitrary command, configure the completion feature just for that command.

zstyle ':completion:*:complete:hahe:*:*' menu yes
zstyle ':completion:*:complete:hahe:*:*' file-patterns '*.(cpp|c)'

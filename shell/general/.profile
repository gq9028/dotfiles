# Environment variables {{{1

export my__root=$HOME
	export my__vim=$my__root/.vim
		export my__vim_snippets=$my__vim/ultisnips
	export my__files=$my__root/my
		export my__books=$my__files/books
		export my__notes=$my__files/notes
		export my__wallpapers=$my__files/wallpapers
		export my__music=$my__files/music
		export my__programs=$my__files/d__programs
		export my__remote_repos=$my__files/remote_repos
			export my__dot=$my__remote_repos/dotfiles
				export my__os=$my__dot/.os/gnulinux
					export my__scripts=$my__os/scripts
				export my__txt=$my__dot/.txt
					export my__resources=$my__txt/resources
					export my__templates=$my__txt/templates
					export my__makefiles=$my__txt/makefiles
					export my__skeletons=$my__txt/skeletons

export my__experiments="$HOME/Experiments"
export my__loggers="$HOME/Loggers"

export EDITOR=vim
export BROWSER=firefox
export KEYTIMEOUT=1

# PATH {{{1

# node

export PATH=$HOME/.npm/bin:$PATH

# python

export PYTHONUSERBASE=$HOME/.pip
export PATH=$HOME/.pip/bin:$PATH

# texlive

export PATH=/usr/local/texlive/2019/bin/x86_64-linux:$PATH

# personal

export PATH=$HOME/.bin:$PATH

# Display server {{{1

if systemctl -q is-active graphical.target \
	&& [[ "x" == "x$DISPLAY" ]] \
	&& [[ $XDG_VTNR == 1 ]] \
	&& [[ $(whoami) != "root" ]]
then
	exec startx
fi

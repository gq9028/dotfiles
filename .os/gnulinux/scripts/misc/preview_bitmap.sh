#!/bin/dash

# Usage
#
# FONT_PATH=/usr/share/fonts/misc ./preview_bitmap.sh
# FONT_PATH=$HOME/.local/share/fonts ./preview_bitmap.sh
#
# For testing a single font, use the following command instead
# export FONT='-misc-fixed-medium-r-normal--20-200-75-75-c-100-iso10646-1' && urxvt -fn "$FONT" -fb "$FONT" -fi "$FONT" -fbi "$FONT" -e tmux attach-session -t preview

[ -z "$FONT_PATH" ] && exit 1

tail -n +2 "$FONT_PATH/fonts.dir" | while read -r line
do
	filename="${line%% *}"
	x_logical="${line#* }"

	echo "$filename"
	urxvt \
		-fn "$x_logical" \
		-fb "$x_logical" \
		-fi "$x_logical" \
		-fbi "$x_logical" \
		-e tmux attach-session -t preview
done

import os

leader_key = "\\"

# Default website {{{1

c.url.default_page = "about:black"
c.url.start_pages = c.url.default_page

# Zoom {{{1

# If the zoom is different from "100%" when setting marks, then marks will be
# created in incorrect positions. I need to post an issue on the  official
# remote repository about this. If it were't for this, then I would be using
# lesser zoom value.
#c.zoom.default = "80%"

config.bind("K", "zoom-in -q")
config.bind("J", "zoom-out -q")

# Font {{{1

# Since I only have 2 bitmap fonts installed in my system ("5x7" and "4x6") and
# the one with # higher priority is "5x7" (the one I want to use here), I can
# set the font to use just by using "misc fixed".
#
# The "0pt" size parameter has been written in order to make the font to be
# applied in all the configuration options. If I had written "misc fixed", then
# it wouldn't have been applied to all the fonts, just to the "c.fonts.tabs"
# configuration option.

preferred_font = "6pt Misc Fixed"

c.fonts.completion.category = preferred_font
c.fonts.completion.entry = preferred_font
c.fonts.contextmenu = preferred_font
c.fonts.debug_console = preferred_font
c.fonts.downloads = preferred_font
c.fonts.keyhint = preferred_font
c.fonts.messages.error = preferred_font
c.fonts.messages.info = preferred_font
c.fonts.messages.warning = preferred_font
c.fonts.prompts = preferred_font
c.fonts.statusbar = preferred_font
c.fonts.tabs.selected = preferred_font
c.fonts.tabs.unselected = preferred_font
c.fonts.hints = preferred_font

# c.fonts.web.size.default = 12
c.content.user_stylesheets = os.getenv("HOME") + "/.config/qutebrowser/main.css"

# Aliases {{{1

c.aliases = {
    "t": "tab-focus",
    "m": "message-info",
    "h": "help"
}

# History {{{1 

config.bind("sh", "history")
config.bind("sH", "history -t")

# Search engines {{{1

c.url.searchengines = {
    # "DEFAULT": "https://duckduckgo.com/?q={}",
   "DEFAULT": "https://duckduckgo.com/?q={}",

    # (a) arch linux
    "a": "https://wiki.archlinux.org/index.php?search={}",
    "a-p": "https://www.archlinux.org/packages/?q={}&sort=pkgname",

    # (d) dotfiles
    "d-a": "https://www.google.com/search?q=site%3Agithub.com%20OR%20site%3Agitlab.com%20dotfiles%20OR%20configs+{}",
    "d-h": "https://google.com/search?q=site%3Agithub.com+%22dotfiles%22+{}",
    "d-l": "https://google.com/search?q=site%3Agitlab.com+%22dotfiles%22+{}",
    "d-s": "https://google.com/search?q=site%3Ahg.sr.ht+%22dotfiles%22+{}",
    "d-n": "https://google.com/search?q=site%3Anest.pijul.com%20%22dotfiles%22+{}",

    # (do) documentation
    "do-g": "https://google.com/search?q=site%3Agit-scm.com+{}",
    "do-z": "https://google.com/search?q=site%3Azsh.sourceforge.net/Doc/Release/+{}",

    # (g) google
    "g-4": "https://google.com/search?q=site%3Aboards.4chan.org+{}",
    "g-h": "https://google.com/search?q=site%3Agithub.com+{}",
    "g-i": "https://google.com/search?q={}&tbm=isch",
    "g-in": "https://google.com/search?q=%22inkscape%22+{}",
    "g-l": "https://google.com/search?q=%22linux%22+{}",
    "g-l-a": "https://google.com/search?q=linux%20arch%20{}",
    "g-st": "https://google.com/search?q=site%3Astackexchange.com+{}",
    "g-l-g": "https://google.com/search?q=linux%20gentoo%20{}",
    "g-r": "https://google.com/search?q=site%3Areddit.com+{}",
    "g-t": "https://google.com/search?q=site%3Atwitter.com+{}",
    "g-w-a": "https://google.com/search?q=site%3Awiki.archlinux.org+{}",
    "g-w-g": "https://google.com/search?q=site%3Awiki.gentoo.org+{}",

    # (gh) github
    "gh-l": "https://github.com/topics/{}?o=desc&s=updated",
    "gh-t": "https://github.com/topics/{}?o=desc&s=stars",
    "gh-u": "https://github.com/{}",

    # (gg) github gists
    "gg-l": "https://gist.github.com/search?o=desc&q={}&s=updated",
    "gg-p": "https://gist.github.com/search?o=desc&q={}&s=stars",
    "gg-u": "https://gist.github.com/{}",

    # (gl) gitlab
    "gl-s-l": "https://gitlab.com/explore?name={}&sort=latest_activity_desc",
    "gl-s-t": "https://gitlab.com/explore?name={}&sort=stars_desc",
    "gl-u": "https://gitlab.com/{}",

    # (l) lbry
    "l": "https://lbry.tv/$/search?q={}",
    "l-d": "https://lbry.tv/$/discover?t={}",

    # (r) reddit
    "r-s": "https://reddit.com/r/{}",
    "r-s-s": "https://subredditstats.com/r/{}",
    "r-u": "https://www.reddit.com/user/{}",

    # (s) search engines
    "s-g": "https://www.google.com/search?q={}",
    "s-s": "https://www.startpage.com/sp/search?query={}",

    # (st) stack exchange
    "st": "https://www.google.com/search?q=site%3Astackexchange.com%20{}",
    "st-s": "https://www.google.com/search?q=site%3Asecurity.stackexchange.com%20{}",
    "st-e": "https://www.google.com/search?q=site%3Aemacs.stackexchange.com%20{}",
    "st-el": "https://www.google.com/search?q=site%3Aelectronics.stackexchange.com%20{}",
    "st-sr": "https://www.google.com/search?q=site%3Asoftwarerecs.stackexchange.com%20{}",

    # (so) soundcloud
    "so": "https://soundcloud.com/search?q={}",
    "so-p": "https://soundcloud.com/search/sets?q={}",
    "so-u-p": "https://soundcloud.com/{}/sets",
    "so-u-a": "https://soundcloud.com/{}/albums",

    # (t) twitter
    "t-h-l": "https://twitter.com/hashtag/{}?f=live",
    "t-h-t": "https://twitter.com/hashtag/{}",
    "t-s": "https://twitter.com/search?q={}",
    "t-u": "https://twitter.com/{}",

    # (v) videos
    "v": "https://invidious.snopyta.org/search?q={}",
    "le": "https://dev.lemmy.ml/search/q/{}/type/all/sort/topall/page/1",

    # non-classified
    "4": "https://boards.4chan.org/{}",
    "w": "https://en.wikipedia.org/wiki/{}",
    "y": "https://youtube.com/results?search_query={}",
    "sc": "https://github.com/koalaman/shellcheck/wiki/SC{}",
    "cpp": "https://en.cppreference.com/mwiki/index.php?search={}",

    # languages
    "en-c": "https://conjugator.reverso.net/conjugation-english-verb-{}.html",
    "en-d": "https://ldoceonline.com/dictionary/{}",
    "en-t": "https://deepl.com/translator#en/es/{}",
    "es-c": "https://conjugador.reverso.net/conjugacion-espanol-verbo-{}.html",
    "es-d": "https://dle.rae.es/?w={}",
    "es-t": "https://deepl.com/translator#es/en/{}",
}

# Tabs {{{1

c.tabs.padding = {
    "bottom": 0,
    "left": 0,
    "right": 0,
    "top": 0
}

c.tabs.indicator.padding = {
    "top": 0,
    "right": 0,
    "bottom": 0,
    "left": 0
}

c.tabs.favicons.show = 'never'

config.bind("<Alt-Tab>", "tab-focus last")
config.bind("<Alt-Shift-l>", "tab-move +")
config.bind("<Alt-Shift-h>", "tab-move -")
config.bind("<Ctrl-g>0", "tab-pin ;; tab-move +1")
config.bind("<Ctrl-g>$", "tab-pin ;; tab-move -1")
config.bind("<Alt-l>", "tab-next")
config.bind("<Alt-h>", "tab-prev")

# Hints {{{1

c.colors.hints.bg = "#ffffff"
c.colors.hints.fg = "#000000"
c.colors.hints.match.fg = "#ffffff"
c.hints.border = "1px solid black"
c.hints.uppercase = True

# Status bar {{{1

c.statusbar.widgets = ["progress", "keypress", "scroll", "history"]
c.statusbar.padding = {
    "bottom": 0,
    "left": 0,
    "right": 0,
    "top": 0
}

# Command line {{{1

c.completion.web_history.max_items = 10
c.completion.open_categories = ["searchengines", "quickmarks", "bookmarks"]
c.completion.height = "10%"

# Mappings {{{1

# Mode: insert {{{2

config.bind("<Ctrl-h>", "fake-key <Backspace>", mode="insert")
config.bind("<Ctrl-w>", "fake-key <Ctrl-Backspace>", mode="insert")
config.bind("<Ctrl-j>", "fake-key <Return>", mode="insert")
config.bind("<Ctrl-g>", "fake-key <Ctrl-a> ;; fake-key <Backspace>", mode="insert")
config.bind("<Ctrl-n>", "fake-key <Down>", mode="insert")
config.bind("<Ctrl-p>", "fake-key <Up>", mode="insert")

# Mode: command {{{2

config.bind("<Alt-j>", "completion-item-focus --history next", mode="command")
config.bind("<Alt-k>", "completion-item-focus --history prev", mode="command")
config.bind("<Ctrl-d>", "rl-delete-char", mode="command")
config.bind("<Ctrl-w>", "rl-backward-kill-word", mode="command")

# Mode: normal {{{2

config.bind(";b", "hint --rapid all tab-bg")
config.bind("R", "config-source")
config.bind("m", "enter-mode set_mark")
config.bind(leader_key + "dc", "download-clear")
config.bind(leader_key + "hc", "history-clear")

# Command: config-cycle {{{2

config.bind(",cj", "config-cycle -t -p content.javascript.enabled")

# Download {{{2

# General

config.bind(",d", "set downloads.location.directory ~/Downloads ;; hint all download")

# Wallpapers

config.bind(",ws", "set downloads.location.directory $my__wallpapers/sort ;; hint all download")
config.bind(",wl", "set downloads.location.directory $my__wallpapers/landscapes ;; hint all download")
config.bind(",wm", "set downloads.location.directory $my__wallpapers/minimalism ;; hint all download")
config.bind(",wa", "set downloads.location.directory $my__wallpapers/animated ;; hint all download")

# Media {{{2

# youtube.com (video titles must be hinted)
# soundcloud.com (title of the current played track must be hinted)

# Download audio, video or both from current tab
config.bind(leader_key + "dv", "spawn -v youtube-dl -o '$my__media/watch_later/%(uploader)s__%(title)s.%(ext)s' '{url}'")
config.bind(leader_key + "da", "spawn -v youtube-dl -o '$my__media/watch_later/%(uploader)s__%(title)s.%(ext)s' -x '{url}'")
config.bind(leader_key + "dm", "spawn -v youtube-dl -o '$my__music/sort/%(title)s.%(ext)s' -x '{url}'")

# Download audio, video or both from hinted items
config.bind(leader_key + "hda", "hint --rapid all spawn -v youtube-dl -o '$my__media/watch_later/%(uploader)s__%(title)s.%(ext)s' -x '{hint-url}'")
config.bind(leader_key + "hdv", "hint --rapid all spawn -v youtube-dl -o '$my__media/watch_later/%(uploader)s__%(title)s.%(ext)s' '{hint-url}'")
config.bind(leader_key + "hdm", "hint --rapid all spawn -v youtube-dl -o '$my__music/sort/%(title)s.%(ext)s' -x '{hint-url}'")

# Open in mpv
config.bind(leader_key + "m", "spawn "\
    "-v "\
    "mpv "\
    "--ytdl "\
    "--force-window=immediate "\
    "--keep-open=yes "\
    "--ytdl-raw-options=format=worst,sub-lang=en,write-sub=,write-auto-sub= "\
    "'{url}'")

config.bind(leader_key + "hm", "hint all spawn "\
    "-v "\
    "mpv "\
    "--ytdl "\
    "--force-window=immediate "\
    "--keep-open=yes "\
    "--ytdl-raw-options=format=worst,sub-lang=en,write-sub=,write-auto-sub= "\
    "'{hint-url}'")

# Yank {{{2

# Link

config.bind("yl", "hint all yank")
config.bind(leader_key + "yl", "hint --rapid all run message-info {hint-url}")

# Search engine format

config.bind("ysu", "yank inline 'site:{url} '")
config.bind("ysh", "yank inline 'site:{url:host} '")

# Misc

config.bind("yh", "yank inline {url:host}")
config.bind("yn", "yank inline '[]({url})'")
config.bind("yp", "spawn -u vim_plug_format {url}")

# Clipboard {{{2

config.bind("pc", "open -- {clipboard}")
config.bind("pC", "open -t -- {clipboard}")
config.bind("pp", "open -- {primary}")
config.bind("pP", "open -t -- {primary}")

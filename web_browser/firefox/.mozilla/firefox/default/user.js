// Miscelanous {{{1

// Define whether Firefox is allowed to read custom user stylesheets.
// Custom user stylesheets must be located in the "~/.mozilla/firefox/{profile name}/chrome/" folder.

user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

user_pref("signon.rememberSignons", false);

// Unknown but I assume it deletes data.
user_pref("privacy.cpd.downloads", true);

// Unknown but I assume it deletes data.
user_pref("privacy.cpd.passwords", true);	

// user_pref("browser.search.defaultenginename", "Duckduckgo");
// user_pref("browser.urlbar.placeholderName", "DuckDuckGo");

// Developer tools {{{1

// More information

user_pref("devtools.theme", "dark");

// Define which tab must be selected when opening the Developer Tools.
//
// Possible values:
// + `options`: Settings
// + `webconsole`: Console
// + `inspector`: Inspector
// + `jsdebugger`: Debugger
// + `styleeditor`: Style Editor
// + `performance`: Performance
// + `netmonitor`: Network
// + `storage`: Storage
// + `memory`: Memory
// + `dom`: DOM
// + `accessibility`: Accesibility
// + `application`: Application
// + `whatsnew`: What's new

user_pref("devtools.toolbox.selectedTool", "webconsole");

// Make "Dock to Right" be the default option.

user_pref("devtools.toolbox.host", "right");

// Disable context output

user_pref("devtools.webconsole.filter.css", false);
user_pref("devtools.webconsole.filter.debug", false);
user_pref("devtools.webconsole.filter.error", false);
user_pref("devtools.webconsole.filter.info", false);

// Javascript evaluation output

user_pref("devtools.webconsole.filter.log", true);
user_pref("devtools.webconsole.filter.net", false);
user_pref("devtools.webconsole.filter.netxhr", false);
user_pref("devtools.webconsole.filter.warn", false);

// Toggle built-in editor

user_pref("devtools.webconsole.input.editor", false);

// Define whether to show a given tab or not
//
// Possible values:
// + true: The tab is shown.
// + false: The tab is not shown.

user_pref("devtools.styleeditor.enabled", false);
user_pref("devtools.performance.enabled", false);
user_pref("devtools.memory.enabled", false);
user_pref("devtools.netmonitor.enabled", false);
user_pref("devtools.storage.enabled", false);
user_pref("devtools.dom.enabled", false);
user_pref("devtools.accessibility.enabled", false);
user_pref("devtools.application.enabled", false);
user_pref("devtools.whatsnew.enabled", false);

// Enable the 3 pane mode in the inspector
user_pref("devtools.inspector.three-pane-enabled", false);

// Tab that msut be selected when opening the inspector tab

user_pref("devtools.inspector.activeSidebar", "layoutview");

// Clear data on shutdown {{{1

// Define whether entries from the "privacy.clearOnShutdown" classification which are true are to be taken into account.
//
// This option is necessary in order for the options below to work.
//
// Possible values:
// + "true": Preferences from "privacy.clearOnShutdown" whose value is "true" are cleared.
// + "false": Preferences from "privacy.clearOnShutdown" whose value is "true" are not cleared.

user_pref("privacy.sanitize.sanitizeOnShutdown", true);

user_pref("privacy.clearOnShutdown.downloads", true);
user_pref("privacy.clearOnShutdown.history", true);
user_pref("privacy.clearOnShutdown.offlineApps", true);
user_pref("privacy.clearOnShutdown.cache", true);
user_pref("privacy.clearOnShutdown.formdata", true);
user_pref("privacy.clearOnShutdown.openWindows", true);
user_pref("privacy.clearOnShutdown.siteSettings", true);
user_pref("privacy.clearOnShutdown.sessions", true);

// Define whether cookies must be deleted when closing Firefox
// 
// This has been tested by opening an account in the following pages and checking whether the accounts close or not.
// + https://github.com
// + https://gitlab.com
// + https://stackexchange.com
//
// As any option that belongs to the "privacy.clearOnShutdown" classification, it is necessary that "privacy.sanitize.sanitizeOnShutdown" be "true" for this preference to be taken into account (provided that it is "true".)
//
// Possible values:
// + "true": Cookies are deleted.
// + "false": Cookies are not deleted.

user_pref("privacy.clearOnShutdown.cookies", false);

// Clear History {{{1

// You can open the "Clear History" windows dialog by 
// + pressing <Ctrl+Shift+Del>.
// +
//   1. Preferences
//   2. Privacy and Security
//   3. History
//   4. Clear History.
//
// CPD stands for "Clear Private Data".

// Make "Everything" be the default option in the "Time range to clear" dropdown list.
user_pref("privacy.sanitize.timeSpan", 0);

// Make the "Cache" checkbox to be checked by default.
user_pref("privacy.cpd.cache", true);	

// Make the "Cookies" checkbox to be checked by default.
user_pref("privacy.cpd.cookies", true);	

// Make the "Offline Website Data" checkbox to be checked by default.
user_pref("privacy.cpd.offlineApps", true);	

// Make the "Site Preferences" checkbox to be checked by default.
user_pref("privacy.cpd.siteSettings", true);

// Make the "Form & Search History" checkbox to be checked by default.
user_pref("privacy.cpd.formdata", true);	

// Make the "Browsing & Download History" checkbox to be checked by default.
user_pref("privacy.cpd.history", true);	

// Make the "Active Logins" checkbox to be checked by default.
user_pref("privacy.cpd.sessions", true);	

// If "true", this will open a new empty window after pressing the "Ok" button.
user_pref("privacy.cpd.openWindows", false);	

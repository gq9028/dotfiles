" This wipes all existing settings. This means that if a setting in this file
" is removed, then it will return to default. In other words, this file serves
" as an enforced single point of truth for Tridactyl's configuration.

sanitize tridactyllocal tridactylsync

"" Miscelanous

" This key has been unbinded since it replaces the Firefox built-in search tool.

unbind <c-f>

" Because the keybinding "<c-f>" has been unbinded, this has also been unbinded to avoid confusions.
" Use "<c-u>" and "<c-d>" as alternatives to "<c-b>" and "<c-f>", respectively.

unbind <c-b>

" The argument which is passed to the "scrollpage" when pressing "<c-u>" and "<c-f>" is "0.5".
" Using "1" is more effective since it makes the window display more new content. 
" Both "<c-f>" and "<c-b>" passes "1" to the "scrollpage" command but I've unbinded those keys due to reasons explained above.

bind <c-u> scrollpage -1
bind <c-d> scrollpage 1

colourscheme blackwhite

" Newtab {{{1

set newtab about:blank
set homepages ["about:blank"]
bind <c-h> home

" Hint {{{1

set hintnames short

bind f hint 
bind F hint -t

bind ;i hint -c input,textarea

bind ;b hint -b
bind ;B hint -qb

" Search {{{1

bind / fillcmdline find
bind ? fillcmdline find -?

bind n findnext 1
bind N findnext -1

bind \n nohlsearch

" Tab {{{1

bind <c-p> pin

bind <a-Tab> tab #

bind <a-h> tabprev
bind <a-l> tabnext

bind <a-H> tabmove -1
bind <a-L> tabmove +1

bind gd tabduplicate

" Open {{{1

bind O fillcmdline tabopen

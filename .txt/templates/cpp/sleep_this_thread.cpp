#include <iostream>
#include <chrono>
#include <thread>

void sleep_seconds()
{
	std::this_thread::sleep_for(std::chrono::seconds(2));
}

void sleep_seconds_2()
{
	using namespace std::chrono_literals;
	std::this_thread::sleep_for(2s);
}

void sleep_milliseonds()
{
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

int main()
{
	auto start = std::chrono::high_resolution_clock::now();
	sleep_milliseonds();
	auto finish = std::chrono::high_resolution_clock::now();

	std::cout<<"Seconds: "<<std::chrono::duration_cast<std::chrono::seconds>(finish - start).count()<<std::endl; 
	std::cout<<"Seconds: "<<std::chrono::duration<double>(finish - start).count()<<std::endl; 
	return 0;
}

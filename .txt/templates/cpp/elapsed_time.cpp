#include <chrono>
#include <thread>
#include <iostream>

void f()
// Function used to simulate an event that we need to wait in order to make the program to continue working.
{
	std::this_thread::sleep_for(std::chrono::seconds(2));
}

void _1()
// According to Bjarne Stroustrup, 'clock()' returns the number of clock ticks sine the start of the program. So it is said that it measures CPU time, not actual time elapsed.
{
	std::clock_t begin = clock();

	for(int i {0}; i<100000000; i++);

	std::clock_t end = clock();

	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	std::cout<<"_1(): "<<elapsed_secs<<" s\n";

}

void _2()
// Measuring time using the 'chrono' library.
// * To display the elapsed time in seconds you just need to use  'std::chrono::duration<double>(finish - start).count()'.
{
	auto start = std::chrono::high_resolution_clock::now();
	f();
	auto finish = std::chrono::high_resolution_clock::now();

	// Elapsed time in seconds (1.0s = 1.0s)
	// -------------------------------------
	std::cout<<"Seconds: "<<std::chrono::duration_cast<std::chrono::seconds>(finish - start).count()<<std::endl; 
	std::cout<<"Seconds: "<<std::chrono::duration<double>(finish - start).count()<<std::endl; 

	// Elapsed time in milliseconds (1.0s = 0.001 ms)
	// ----------------------------------------------
	// * Used to show how long did an action take to complete in milliseconds.
	std::cout<<"Milliseconds: "<<std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()<<std::endl;
	std::cout<<"Milliseconds: "<<std::chrono::duration<double, std::milli>(finish - start).count()<<std::endl;

	// Elapsed time in microseconds (1.0s = 0,000 001 µs)
	// --------------------------------------------------
	// * Used to show how long did an action take to complete in microseconds.
	std::cout<<"Microseconds: "<<std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count()<<std::endl;
	std::cout<<"Microseconds: "<<std::chrono::duration<double, std::micro>(finish - start).count()<<std::endl;

	// Elapsed time in nanoseconds (1.0s = 0.000 000 001 ns)
	// ----------------------------------------------------
	// * Used to show how long did an action take to complete in nanoseconds.
	std::cout<<"Nanoseconds: "<<std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count()<<std::endl; 
	std::cout<<"Nanoseconds: "<<std::chrono::duration<double, std::nano>(finish - start).count()<<std::endl; 
}

int main()
{
	_2();
	return 0;
}

#ifndef CLASS_SHAPE
#define CLASS_SHAPE

template <typename T>
class Shape
{
	public:
		Shape(const T &x, const T &y);
		~Shape();
	protected:
		T* x;
		T* y;
};

#include "shape.hppt"

#endif // CLASS_SHAPE

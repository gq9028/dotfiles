#ifndef CLASS_POINT
#define CLASS_POINT

#include "shape.hpp"

template <typename T>
class Point : public Shape<T>
{
	public:
		Point(const T &x, const T &y, const T &color);
		~Point();

		void PrintInformation();
	private:
		T* color;
};

#include "point.hppt"

#endif // CLASS_POINT

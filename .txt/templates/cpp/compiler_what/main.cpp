// Errors are only shown when those functions are called.
#include  <iostream>

struct forward_list
{
	struct node
	{
		void kill_self();
	};
};

int main()
{
	forward_list::node a;

	// Undefined reference
	//a.kill_self();
	return 0;
}

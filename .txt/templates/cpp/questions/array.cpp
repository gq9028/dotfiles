#include <iostream>
#include "my_utilities.hpp"

void baa(int*& values)
{
	values = new int [5] {10, 20,30 , 40 , 50};
}

int main()
{
	// It is destroyed automatically.
	// The pointer cannot be passed to a function that receives parameter as references.
	int values1 [] {1,2,3,4,5};
	// It is destroyed manually.
	int* values2 = new int [5] {1, 2, 3, 4, 5};
	
	// baa(values1); // Compilation error
	baa(values2);

	print_array(values1, 5);
	print_array(values2, 5);

	return 0;
}


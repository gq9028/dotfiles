#include <iostream>

class Class
{
	void print() {};
};

struct Struct
{
	void print() {};
};

int main()
{
	Class MyClass;
	Struct MyStruct;	

	// MyClass.print(); // forbidden
	MyStruct.print();

	return 0;
}

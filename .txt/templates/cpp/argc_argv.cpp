#include <iostream>

// argc: argument count
// 	* Number of strings pointed to by argv.
// 	* It will be 1 plus the number of arguments, as virtually all implementations will prepend the name of the program to the array.
// argv: argument vector
// These names were given as a convention. However they can be given any valid identifier.
int main(int argc, char* argv[])
{
	std::cout<<argc<<" arguments detected."<<std::endl;
	for(int i {0}; i < argc; i++)
		std::cout<<"Argument n°"<<i<<": "<<argv[i]<<std::endl;

	return 0;
}


#include <iostream>

// It doesn't work
int getSize1(int items[])
{
	std::cout<<"\n[DEBUG] ███████████████ getSize1()";
	std::cout<<"\n[DEBUG] \"sizeof(items)\" := \""<<sizeof(items)<<"\"";
	std::cout<<"\n[DEBUG] \"sizeof(items[0])\" := \""<<sizeof(items[0])<<"\"";
	std::cout<<"\n[DEBUG] \"sizeof(items)/sizeof(items[0])\" := \""<<sizeof(items)/sizeof(items[0])<<"\"";
	return 0;
}

// It doesn't work.
int getSize2(int* items)
{
	std::cout<<"\n[DEBUG] ███████████████ getSize2()";
	std::cout<<"\n[DEBUG] \"sizeof(items)\" := \""<<sizeof(items)<<"\"";
	std::cout<<"\n[DEBUG] \"sizeof(items[0])\" := \""<<sizeof(items[0])<<"\"";	
	std::cout<<"\n[DEBUG] \"sizeof(items)/sizeof(items[0])\" := \""<<sizeof(items)/sizeof(items[0])<<"\"";
}

int main()
{
	int items [] {1,2,3,4,5};

	std::cout<<"\n[DEBUG] ███████████████ main()";
	std::cout<<"\n[DEBUG] \"sizeof(items)\" := \""<<sizeof(items)<<"\"";
	std::cout<<"\n[DEBUG] \"sizeof(items[0])\" := \""<<sizeof(items[0])<<"\"";
	//
	// It works, just because it is in the same scope as the array initialization.
	//
	std::cout<<"\n[DEBUG] \"sizeof(items)/sizeof(items[0])\" := \""<<sizeof(items)/sizeof(items[0])<<"\"";

	getSize1(items);
	getSize2(items);
}

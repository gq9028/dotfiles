/**
 *
 * Print C and C++ style arrays.
 *
 */
template <typename T, typename Y>
void print_array(T* items, Y size)
{
	std::cout<<"{";
	for(std::size_t i {0}; i < size; i++)
		{
			std::cout<<items[i];
			if(i != size - 1)
				std::cout<<", ";
		}	
	std::cout<<"}";
}

/*
 *
 * Non in-place reverse on C++ style arrays
 *
 */
template <typename T1, typename T2>
void reverse_p_1(T1*& items, T2 size)
{
	T1* temp = new T1 [size];
	for(int i = size - 1; i >= 0; i--)
		temp[size - i - 1] = items[i];
	delete items;
	items = temp;
}

/*
 *
 * In-place reverse on C and C++ style arrays
 *
 */
template <typename T1, typename T2>
void reverse(T1* elements, T2 size)
{
	if(size < 0)
		throw std::runtime_error("reverse");

	if(size == 0 || size == 1)	
		return;

	for(std::size_t i {0}; i < size/2; i++)
		std::swap(elements[i], elements[size - i - 1]);
}

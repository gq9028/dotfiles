#!/bin/bash

foo=(aaaaaaaaa bbbbbbbbb)

# Checking if variable is not set or is empty.

if [ -z "$foo" ]; then
	echo "variable is empty"
else
	echo "variable is not empty"
fi

# Checking if variable is not set or is empty.

if [ ! -z "$foo" ]; then
	echo "variable is not empty"
else
	echo "variable is empty"
fi

# Checking if variable is set

if [ -z ${foo+whatever} ]
then
	echo "foo doesn't exist or is empty or isn't defined"
else
	echo "foo exists"
fi

#!/bin/bash

# Including '/home/onceiusedwindows/'
cp_parents_1()
{
    source_pathname=$1
    target_topdir=$2

    mkdir -p "$target_topdir/${source_pathname%/*}"
	if [ -d $source_pathname ]
		then cp -r $source_pathname/* "$target_topdir/$source_pathname"
		else cp "$source_pathname" "$target_topdir/$source_pathname"
	fi
}

# Excluding '/home/onceiusedwindows/'
cp_parents()
{
	file=$1
	fp_file=${file%/*}
	fp_file=${fp_file:23} #23: length of my root user directory '/home/onceiusedwindows'.

    target_dir=$2

    mkdir -p "$target_dir/$fp_file"
	
	if [ -d $file ]
		then cp -r $file/* "$target_dir/$fp_file"
		else cp "$file" "$target_dir/$fp_file"
	fi
}

cp_parents ~/Documents/MyFiles/dc_GithubRepos/oop_exercises/abstract_datatypes/ .

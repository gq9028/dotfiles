#!/bin/bash

# Set background color using ANSI escape

echo "$(tput setab 0) tput setab 0 "
echo "$(tput setab 1) tput setab 1 "
echo "$(tput setab 2) tput setab 2 "
echo "$(tput setab 3) tput setab 3 "
echo "$(tput setab 4) tput setab 4 "
echo "$(tput setab 5) tput setab 5 "
echo "$(tput setab 6) tput setab 6 "
echo "$(tput setab 7) tput setab 7 "
echo "$(tput setab 0) tput setab 0 "

# Set foreground color using ANSI escape

echo "$(tput setaf 0) tput setaf 1 "
echo "$(tput setaf 1) tput setaf 1 "
echo "$(tput setaf 2) tput setaf 2 "
echo "$(tput setaf 3) tput setaf 3 "
echo "$(tput setaf 4) tput setaf 4 "
echo "$(tput setaf 5) tput setaf 5 "
echo "$(tput setaf 6) tput setaf 6 "
echo "$(tput setaf 7) tput setaf 7 "

# Default color

echo "$(tput sgr0) tput sgr0"

#!/bin/bash

foo=(aa bb)

# Checking if variable is array.

if [[ "$(declare -p foo)" =~ "declare -a" ]]
then
    echo array
else
    echo no array
fi

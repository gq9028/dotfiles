#!/bin/bash

foo=a

echo "[DEBUG] ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ Simple if"

echo "[DEBUG] ══════════════════════════════ Simple"

if [ -z "$foo" ]
then
	echo "variable is empty"
else
	echo "variable is not empty"
fi

echo "[DEBUG] ══════════════════════════════ Simple (semicolons)"

if [ -z "$foo" ];
then
	echo "variable is empty";
else
	echo "variable is not empty";
fi;

echo "[DEBUG] ══════════════════════════════ Single line then and else"

if [ -z "$foo" ]
	then echo "variable is empty"
	else echo "variable is not empty"
fi;

echo "[DEBUG] ══════════════════════════════ Single line then and else (semicolons)"

if [ -z "$foo" ];
	then echo "variable is empty";
	else echo "variable is not empty";
fi;

echo "[DEBUG] ══════════════════════════════ Single line if statement"

if [ -z "$foo" ]; then echo "variable is empty" ; else echo "variable is not empty" ; fi

echo "[DEBUG] ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ Ternary operator"

[ -z "$foo" ] && echo "variable is empty" || echo "variable is not empty"

echo "[DEBUG] ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ if test"

echo "[DEBUG] ══════════════════════════════ Simple"

if test -z "$foo"
then
	echo "variable is empty"
else
	echo "variable is not empty"
fi

echo "[DEBUG] ══════════════════════════════ Simple (semicolons)"

if test -z "$foo";
then
	echo "variable is empty";
else
	echo "variable is not empty";
fi;

echo "[DEBUG] ══════════════════════════════ Single line"

if test -z "$foo"; then echo "variable is empty"; else echo "variable is not empty"; fi;

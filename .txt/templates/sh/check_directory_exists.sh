#!/bin/bash

my_dir="./hola"

# Checking if directory exists

if [ -d $my_dir ]; then
	echo 1: Directory exists
else
	echo 1: Directory doesn\'t exist
fi

# Checking if directory doesn't exist

if [ ! -d $my_dir ]; then
	echo 2: Directory doesn\'t exist
else
	echo 2: Directory exists
fi

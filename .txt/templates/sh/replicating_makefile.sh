#!/bin/bash

case $# in
	0)
		# 
		# If no argument is passed, 're' is the function to execute.
		# 
		to_execute=re
		root=main.cpp
		;;
	1)
		# 
		# First argument must be name of the function to execute.
		# 
		to_execute=re
		root=$1
		;;
	2)
		#
		# First argument must be name of the function to execute.
		# Second argument must be name of the root file.
		# 
		to_execute=$1
		root=$2
		;;
	*)
		echo "This script is only executed when 0, 1 or 2 arguments is executed."
		exit 1
		;;
esac

CXX=g++
CXXFLAGS=--std=c++11
OUT=a.out

all()
{
	echo "Compiling using \"$root\" as root file..."
	$CXX $CXXFLAGS $root -o $OUT
	if [ $? -eq 1 ]
	then
		exit 1
	fi
}

clean()
{
	echo "Deleting \"$OUT\" file..."
	rm -rf $OUT
}

exe()
{
	clear
	./a.out
}

re()
{
	clean	
	all
	exe
}

$to_execute

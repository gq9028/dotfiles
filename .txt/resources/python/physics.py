"""Physics utilities"""

# Proton mass, electron mass and neutron mass
# Extracted from: b:en:young:up:13, p. 689

m_proton = 1.672621637 * 10 ** -27
m_electron = 9.10938215 * 10 ** -31
m_neutron = 1.674927211 * 10 ** -27

import csv


def get_headers(csv_file):
    from itertools import islice

    with open(csv_file) as cf:
        cr = csv.reader(cf)
        print(next(islice(cr, 0, 1)))

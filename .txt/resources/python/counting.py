"""Counting utilities"""

from math import factorial


def permutation(n: int, r: int) -> int:
    """
    Returns the number of r-permutation of a set with n elements.
    """

    if n < r:
        raise Exception(f'r must not exceed n. (n = {n}) (r = {r})')
    if r == 0 or n == 0:
        return 1
    if r == 1:
        return n
    if r == n:
        return factorial(n)

    return factorial(n) / factorial(n - r + 1)

p: lambda n, r: permutation(n, r)

#pragma once

#include <iostream>

auto debug_print = [](const char *name, auto &var,
                      std::ostream &out = std::clog) {
  out << name << " := \"" << var << "\"\n";
};

#define DEBUG_PRINT(var) debug_print(#var, var, std::cout)

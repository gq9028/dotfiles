#include <random>
#include <iostream> // cout
#include <limits> // numeric_limits

// Get the boundaries of a given data type.
// T represents the data type
// Y represents the type of data to which T should be casted.
template <typename T, typename Y = T>
void Boundaries()
{
	std::cout	<<"["
						<<Y(std::numeric_limits<T>::min())
						<<", "
						<<Y(std::numeric_limits<T>::max())
						<<"]";
}


// Generates a random int in the interval [a,b].
// GetRandomInt()       = GetRandomInt(-2147483648, 2147483647)
// GetRandomInt(10)     = GetRandomInt(10, 2147483647)
// GetRandomInt(10, 20) = GetRandomInt(10, 20)
int GetRandomInt(int a = std::numeric_limits<int>::min(), int b = std::numeric_limits<int>::max())
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(a, b);
	return dis(gen);
}

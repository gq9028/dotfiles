#include <iostream>
#include <vector>

template<typename T>
std::ostream&
operator<<(std::ostream& os, const std::vector<T>& items)
{
	os << "{";
	for (std::size_t i{ 0 }; i < items.size(); i++) {
		os << items[i];
		if (i != items.size() - 1)
			os << ", ";
	}
	os << "}";
	return os;
}

TEX_BIN ?= pdflatex
TEX_BIN_FLAGS ?= --shell-escape

check:
	@[ ${SRC} ] || exit 1
	@[ ${OUT_DIR} ] || exit 1

compile: check
	cd $(OUT_DIR) && $(TEX_BIN) $(TEX_BIN_FLAGS) $(SRC)

biber: check
	cd $(OUT_DIR) && $(TEX_BIN) $(TEX_BIN_FLAGS) $(SRC)
	cd $(OUT_DIR) && biber $(SRC)
	cd $(OUT_DIR) && $(TEX_BIN) $(TEX_BIN_FLAGS) $(SRC)

glossary: check
	cd $(OUT_DIR) && $(TEX_BIN) $(TEX_BIN_FLAGS) $(SRC)
	cd $(OUT_DIR) && makeglossaries $(SRC)
	cd $(OUT_DIR) && $(TEX_BIN) $(TEX_BIN_FLAGS) $(SRC)

clean:
	find . \
		\( \
			\( \
				-type f \
				-a \( \
					-name '*.aux' \
					-o -name '*.acn' \
					-o -name '*.acr' \
					-o -name '*.alg' \
					-o -name '*.bbl' \
					-o -name '*.bcf' \
					-o -name '*.blg' \
					-o -name '*.glg' \
					-o -name '*.glo' \
					-o -name '*.gls' \
					-o -name '*.ist' \
					-o -name '*.listing' \
					-o -name '*.log' \
					-o -name '*.nav' \
					-o -name '*.out' \
					-o -name '*.run.xml' \
					-o -name '*.snm' \
					-o -name '*.toc' \
					-o -name '*.vrb' \
					-o -name 'tmp.*' \
				\) \
			\) \
			-o \
			\( \
				-type d \
				-a \
				-name '_minted-*' \
			\) \
		\) \
		-exec rm -rf {} +

mymain: compile

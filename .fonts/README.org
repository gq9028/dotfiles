* Bitmap fonts

~4x6.pcf.gz~ and ~5x7.pcf.gz~ were retrieved from [[https://github.com/jfhbrook/pxvncfb/tree/master/fonts][this repositokhry]].

* Truetype fonts

| Name             | Remote repository |
|------------------+-------------------|
| DejaVu Sans Mono | [[https://github.com/dejavu-fonts/dejavu-fonts][URL]]               |
| FiraCode         | [[https://github.com/tonsky/FiraCode][URL]]               |
| Hack             | [[https://github.com/source-foundry/Hack][URL]]               |

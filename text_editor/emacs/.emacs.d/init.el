;; Previous configuration

; (defun copy-file-name ()
;   (interactive)
;   (kill-new (buffer-file-name)))

; (global-set-key (kbd "M-i c")
;   (lambda () (interactive)
;     (message "%d" (+ (current-column) 1))))

; ; Vanilla Emacs

; ; Micelanous

; (setq dired-listing-switches "-Al --group-directories-first")

; (global-set-key (kbd "C-x C-,") 'eval-buffer)

; (line-number-mode -1)
; (fringe-mode 0)

; ; Disable cursor blinking.

; (blink-cursor-mode 0)

; (define-key minibuffer-local-map (kbd "C-p") 'previous-line-or-history-element)
; (define-key minibuffer-local-map (kbd "C-n") 'next-line-or-history-element)

; ; Each of the following options become buffer-local when set. Because of this, it is necessary to use ~set-default~ instead of ~setq~.

; ; Find more information in [[https://www.emacswiki.org/emacs/TruncateLines][the "TruncateLines" article in Emacs Wiki]].
; ; TODO What's the difference betweeen ~setq-default~ and ~set-default~?
; ;(setq-default truncate-lines t)

; (set-default 'truncate-lines t)
; (set-default 'word-wrap nil)
; (set-default 'indent-tabs-mode nil)
; (set-default 'indicate-empty-lines nil)
; (set-default 'tab-width 4)

; ;;; Keymaps

; ; Discussion on this method can be found in scottfrazer's answer to [[https://stackoverflow.com/questions/683425/globally-override-key-binding-in-emacs][this question in Stack Overflow]].

; ; TOOD Make keys be always enabled except in the minibuffer.

; ;; * Miscelanous

; ;; More information on disabled commands in ~(emacs) Disabling~.

; ;; #+begin_src emacs-lisp
; ;; #+end_src

; ;; #+begin_src emacs-lisp
; ;; (setq backup-directory-alist `(("." . "~/.emacs.d/backup")))
; ;; #+end_src

; ;; * Appearancek

; ;; ** GUI

; ;; #+begin_src emacs-lisp
; ;; (tool-bar-mode -1)
; ;; (menu-bar-mode -1)
; ;; (toggle-scroll-bar -1)
; ;; #+end_src

; ;; * Packages

; ;; ** ~org~

; ;; #+begin_src emacs-lisp
; ;; (setq org-agenda-files
; ;;   (directory-files "~/my/org" 1 "^[^\.].*\\.org$" 1))
; ;; #+end_src

; ;; #+begin_src emacs-lisp
; ;; (setq org-todo-keywords '(
; ;;   (sequence "TODO(y@)" "DOING(h)" "|" "DONE(n@)")
; ;;   (sequence "IMPROVE(u@)" "IMPROVING(j)" "|" "IMPROVED(m@)")
; ;;   (sequence "WAITING(i@)" "|" "CANCELLED(k@)")))
; ;; #+end_src

; ;; #+begin_src emacs-lisp
; ;; (setq org-time-stamp-formats '("<%Y-%m-%d %a>" . "<%Y-%m-%d %a %H:%M:%S>"))
; ;; #+end_src

; ;; ** ~org-journal~

; ;; #+begin_src emacs-lisp
; ;; (setq org-journal-dir "~/my/journal/")
; ;; (setq org-journal-file-format "%Y_%m_%d_%a")
; ;; (setq org-journal-date-format "%d/%m/%Y (%a)")
; ;; (setq org-journal-time-format "")
; ;; (require 'org-journal)
; ;; #+end_src

; ;;; Configuration togglers

; (global-set-key (kbd "M-t") nil)

; (global-set-key (kbd "M-t h")
;   (lambda () (interactive)
;     (if hl-line-mode
;       (hl-line-mode -1)
;       (hl-line-mode))))

; (global-set-key (kbd "M-t H")
;   (lambda () (interactive)
;     (if global-hl-line-mode
;       (global-hl-line-mode -1)
;       (global-hl-line-mode))))

; (global-set-key (kbd "M-t f")
;   (lambda () (interactive)
;     (if font-lock-mode
;       (font-lock-mode -1)
;       (font-lock-mode))))

; (global-set-key (kbd "M-t F")
;   (lambda () (interactive)
;     (if global-font-lock-mode
;       (global-font-lock-mode -1)
;       (global-font-lock-mode))))

;  (global-set-key (kbd "M-i") nil)

; ------------


; Miscelanous

; (put 'upcase-region 'disabled nil)

; (use-package paren
;   :config
;     (show-paren-mode))

; (use-package vc-hooks
;   :config
;     ; As for Emacs 27.1, this behavior is not set when both the symbolic link and its corresponding file both are under version control. They necessarily don't need to be under the same version control environment.
;     (setq vc-follow-symlinks nil))

; ;; (use-package startup
; ;;   :config
; ;;     (setq inhibit-startup-screen t))

; ;; (use-package windmove
; ;;   :bind
; ;;     (("C-h" . windmove-left)
; ;;     ("C-j" . windmove-down)
; ;;     ("C-k" . windmove-up)
; ;;     ("C-l" . windmove-right)))

; (use-package org
;   :config
;     (setq org-structure-template-alist
; 	  '(("s" . "src")
; 	    ("E" . "src emacs-lisp")
; 	    ("e" . "example")
; 	    ("q" . "quote")
; 	    ("v" . "verse")
; 	    ("V" . "verbatim")
; 	    ("c" . "center")
; 	    ("C" . "comment")))
;     ; In some systems, name for all available languages is present in the directory "/usr/share/emacs/27.1/lisp/org".
;     ; Specific information for the "C++" language is present in "(org) Languages".
;     (org-babel-do-load-languages 'org-babel-load-languages
; 				 '((shell      . t)
; 				   (js         . t)
; 				   (C          . t)
; 				   (emacs-lisp . t)
; 				   (python     . t))))

;; -----------
;; Miscelanous

; The following configuration unmaps the =<f1>= key since =C-h= can be used instead.

(global-set-key (kbd "<f1>") nil)

; The following mapping is useful for visiting files on the fly that are listed after
; + executing =find-dired=.
; + listing the files with the =pkgfile= command.

(global-set-key (kbd "C-x f") 'find-file-at-point)

; Make backup files be always created in 

(setq backup-directory-alist `(("." . "~/emacs/saves")))

(require 'shell)

(defun fc/shell-switch-dir ()
  "Switch `shell-mode' to the `default-directory' of the last buffer."
  (interactive)
  (when (eq major-mode 'shell-mode)
    (let* ((dir (catch 'return
                  (dolist (buf (buffer-list))
                    (with-current-buffer buf
                      (when buffer-file-name
                        (throw 'return
                               (expand-file-name default-directory))))))))
      (goto-char (process-mark (get-buffer-process (current-buffer))))
      (insert (format "cd %s" (shell-quote-argument dir)))
      (let ((comint-eol-on-send nil))
        (comint-send-input)))))

(define-key shell-mode-map (kbd "C-c C-y") 'fc/shell-switch-dir)


; This function is used in the ":dir" header arguments of shell code blocks as well as the ":tangle" header arguments of some code blocks

(defun in-experiments (&optional subpath)
  (if subpath
      (concat path/experiments "/" subpath) path/experiments))

; C-k must delete entire lines

(setq kill-whole-line t)

; Retrieve packages from MELPA repositories

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

; Sets theme

; These are the themes that I like: 
; + Light themes: leuven, tsdh-light
; + Dark themes: manoj-dark, tango-dark, deeper-blue, tsdh-dark
; wheatgrass
; whiteboard
(load-theme 'whiteboard)

; Sets the default font.

(set-face-attribute 'default nil :family "Misc Fixed" :height 60)

; Disable startup screen

(setq inhibit-startup-screen t)

; Changes the format of the information that is displayed when opening a directory

(setq dired-listing-switches "-lAv --group-directories-first")

; Highlight matching parentheses

(show-paren-mode)

; I use "M-t" as a prefix for keys that toggle configuration options. For example "M-t n" and "M-t t" toggles line numbering and truncate lines, respectively.

(global-set-key (kbd "M-t") nil)

; I use "M-i" as a prefix for information keys. For example "M-i m", "M-i l", "M-i L", shows the current major mode, current line, number of lines in the current buffer, respectively.

(global-set-key (kbd "M-i") nil)

; Message the name of all active minor modes

(global-set-key (kbd "M-i m")
  (lambda () (interactive)
    (message "%s" minor-mode-list)))

; Message the name of the major-mode

(global-set-key (kbd "M-i M")
  (lambda () (interactive)
    (message "%s" major-mode)))

; Message the name of the file-name associated to the current buffer

(global-set-key (kbd "M-i f")
  (lambda () (interactive)
    (message "%s" (buffer-file-name))))

; Message current line numbering

(global-set-key (kbd "M-i l")
  (lambda () (interactive)
    (message "%d" (line-number-at-pos))))

; Message number of lines in the current buffer

(global-set-key (kbd "M-i L")
  (lambda () (interactive)
    (save-excursion
      (let (beg end total)
        (forward-page)
        (beginning-of-line)
        (or
          (looking-at page-delimiter)
          (end-of-line))
        (setq end (point))
        (backward-page)
        (setq beg (point))
        (setq total (count-lines beg end))
        (message "%d" total)))))

; Message current working directory

(global-set-key (kbd "M-i d")
                (lambda () (interactive)
                  (message "%s" default-directory)))

; I constantly use this function
; I've used "C-." as a prefix because I still don't know how to overwrite org-mode keys and this key was not bound to any functino. In addition to this, this  key is not used by other keys.

(global-set-key (kbd "C-. r")
  (lambda () (interactive)
    (revert-buffer nil t)))


; I use this function to set sequen
; Further discussion on: https://emacs.stackexchange.com/questions/51399/org-babel-tangle-with-function-in-header-arguments

(defvar-local snippets-file-i 0
  "Counter for snippets file")

(add-hook 'org-babel-pre-tangle-hook          
  (lambda ()
    (setq-local snippets-file-i 0)))

(defun snippets-file-name () (format "%d" (cl-incf snippets-file-i)))

;;; Enabled functions

; The following configurations enable some of the functions which are disabled, by default.

(put 'set-goal-column 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

; The following function is useful for getting rid of a large buffer since the content is not saved in the kill ring.

(put 'erase-buffer 'disabled nil)

;;; Paths

; The following variable stores the Emacs configuration directory

(setq path/emacs "~/.emacs.d")

; The following variable stores my custom defined yasnippet's snippets.

(setq path/snippets (concat path/emacs "/snippets"))

; The following variable stores the path where I experiment with programming languages, commands, etc. For eaxmple, this is the directory in which some files are tangled by Org Mode.
;
; I could have used =(getenv "my__experiments")= straight in all the places where I use this variable but then when I decide to change this directory, it will be more difficult to perform all the replacements.

(setq path/experiments (getenv "my__experiments"))

; ".bib" files

(setq path/bib "~/my/bib")

; The following path stores the directory in which I store multiple configurations of other Emacs users. I store configurations of some other users because I like to read them and retrieve ideas which I find useful.

(setq path/configs "~/SourceCode/dotfiles/emacs")

;; Mode line

; Define appearance of the current line number in the status bar.

(line-number-mode 0)

;; Line numbering

; Most of the times, I don't consider line numbering useful. For this reason, it is disabled by default.

(setq-default display-line-numbers nil)

; If I need to look at line numbers, then I can easily toggle it by triggering the following keybinding.

(global-set-key (kbd "M-t n")
                (lambda () (interactive)
                  (display-line-numbers-mode 'toggle)))

; Disable font lock mode in the current buffer

(global-set-key (kbd "M-t f")
                (lambda () (interactive)
                  (font-lock-mode 'toggle)))
                 
;; Truncation

(setq-default truncate-lines t)

; Toggle line truncation in the current buffer.

(global-set-key (kbd "M-t t")
  (lambda () (interactive)
    (toggle-truncate-lines)
    (adaptive-wrap-prefix-mode)))

;; Indentation

; I prefer spaces over tabs

(setq-default
  tab-width 2
  indent-tabs-mode nil)

; By default, "js-indent-level" is 4. However, I prefer indentation be 2 spaces.
; References: https://stackoverflow.com/questions/17901900/how-can-i-set-a-tab-width-for-json-files
; TODO: Why creating a hook? Wouldn't it be better to assign the value to the variable straight?

(setq js-indent-level 2)

; When Emacs cannot recognize the indentation level, I would like it to be 2.

(setq python-indent-offset 2)

; The following code block has been saved since it will be replaced with a makefile hook, since makefile requires using tabs.

(add-hook 'emacs-lisp-mode-hook
  (lambda ()
    (setq-local tab-width 2
                indent-tabs-mode nil)))

;; Custom modes

;;; YAML mode

(define-derived-mode yaml-mode fundamental-mode "YAML"
  "Major mode for editing YAML files."
  (setq indent-tabs-mode nil
        tab-width 2))

(add-to-list 'auto-mode-alist (cons (purecopy "\\.ya?ml\\'") 'yaml-mode))

;; Packages

;;; man

; The following configuration set the regular expression that is used 
;
; + by the function =Man-next-section= and =Man-next-section= in order to compute the jump position of the cursor.
; + by the function =Man-goto-section= to list all the available sections.
;
; The default configuration didn't include parentheses. These characters are used in the =pacman= manual page. For this reason, they are now included.

(setq Man-heading-regexp "^\\([[:upper:]][[:upper:]0-9 ()/-]+\\)$")

;;; tex-mode

; The following configuration disables fontification for subscript and superscripts.
;
; This configuration was retrieved from [[https://emacs.stackexchange.com/questions/35782][this question at Emacs Stack Exchange]].

(setq tex-fontify-script nil)

;;; frame

; The following configuration disables cursor blinking.

(blink-cursor-mode 0)

;;; scroll-bar

; The following configuration hides the scrollbar in all frames.

(set-scroll-bar-mode nil)

;;; menu-bar

; The following configuration hides the menu bar.

(menu-bar-mode 0)

;;; tool-bar

; The following configuration hides the tool-bar.

(tool-bar-mode 0)

;;; outline


(add-hook 'emacs-lisp-mode-hook
 (lambda ()
   (setq-local outline-regexp ";;;* ")))

;;; org

; In some systems, name for all available languages is present in the directory "/usr/share/emacs/27.1/lisp/org". Therefore, you can list them by executing the following command
; find /usr/share/emacs/27.1/lisp/org -name 'ob-*.elc'
;
; Specific information for the "C++" language is present in "(org) Languages".

(org-babel-do-load-languages 'org-babel-load-languages
                             '((C . t)
                               (python . t)
                               (latex . t)
                               (js . t)
                               (maxima . t)
                               (gnuplot . t)
                               (calc . t)
                               (shell . t)))


; I don't want spaces to be added at the beginning of each line of source code blocks after finishing editing the buffer

(setq org-edit-src-content-indentation 0)

; The following configuration sets the scale for LaTeX images presented in an Org buffer.

(plist-put org-format-latex-options :scale 1.5)

; dvipng does not load packages while imagemagick does
; more information on this https://emacs.stackexchange.com/questions/60696/make-org-latex-preview-load-package-so-that-it-properly-renders-tcolorbox-en
;
; You can create you own latex preview process. Here's an example: https://github.com/green-93-nope/private_layer/blob/7fcfc922a09a2fd264a075e88a25a7feb312d7ea/green-org/packages.el
;
; TODO: Citation must work when previewing

(setq org-preview-latex-default-process 'imagemagick)


; Otherwise, we get the following error: Symbol's value as variable is void: org-latex-minted-langs


; I don't want to load any package when exporting documents

(setq org-latex-default-packages-alist '())


; The following configuration disables syntax highlighting in all =#+BEGIN_SRC= code blocks
;
; I have disabled it because of the following reasons
; + I think that it heavily affects the performance Emacs performance.
; + Most of the time I don't need syntax highlighting. The goal of syntax highlighting is to make the content of a buffer be friendly to the human eye. I sometimes don't need syntax highlighting because most of the times I find the content to be read easy to understand.
; + The function =org-edit-src-code= (bounded to =C-c '=) can be used as a workaround to enable syntax highlighting in a given =#+BEGIN_SRC= block.

(setq org-src-fontify-natively nil)

; Preview LaTeX code blocks

(global-set-key (kbd "C-. l i")
  (lambda () (interactive)
  (setq org-babel-default-header-args:latex '((:results . "file raw") (:file . "tmp.png")))))

; Exports results as LaTeX

(global-set-key (kbd "C-. l l")
  (lambda () (interactive)
    (setq org-babel-default-header-args:latex '((:results . "latex") (:exports . "results")))))

; Some packages such as TikZ & Pgf (specifically, binary tree layout) require luamagick
; (setq org-latex-pdf-process
;   '("lualatex -shell-escape -interaction nonstopmode %f"
;     "lualatex -shell-escape -interaction nonstopmode %f"))
; (setq luamagick '(luamagick
;                   :programs ("lualatex" "convert")
;                   :description "pdf > png"
;                   :message "you need to install lualatex and imagemagick."
;                   :use-xcolor t
;                   :image-input-type "pdf"
;                   :image-output-type "png"
;                   :image-size-adjust (1.0 . 1.0)
;                   :latex-compiler ("lualatex -interaction nonstopmode -output-directory %o %f")
;                   :image-converter ("convert -density %D -trim -antialias %f -quality 100 %O")))
; (add-to-list 'org-preview-latex-process-alist luamagick)
; (setq org-preview-latex-default-process 'luamagick)

; The following mapping makes inserting a timestamp of a video into the current buffer.

(defun mpv-property-string (property)
  (replace-regexp-in-string "\n$" ""
  (shell-command-to-string
   (concat "echo '{ \"command\": [\"get_property_string\", \""
           property
           "\"] }' | socat - /tmp/mpvsocket  | jq -r '.data'"))))

(defun org-insert-link-video () (interactive)
       (insert (concat "[[video:" (mpv-property-string "working-directory") "/" (mpv-property-string "path") "::" (mpv-property-string "time-pos") "]]")))

(define-key org-mode-map (kbd "C-. v") 'org-insert-link-video)

; The following configuration sets the Org agenda files.

(setq org-agenda-files (directory-files-recursively "~/my/org" org-agenda-file-regexp))

; The following configuration sets the width for images rendered in a buffer.

(setq org-image-actual-width 300)

; The following configurations sets the state of the headings.

(setq org-todo-keywords
      (quote ((sequence "TODO(t!/!)" "DONE(d!/!)"))))

; The following configuration defines the algorithm used in the selection of the state of all headings.

(setq org-use-fast-todo-selection 'expert)
                        
;;; org-src

(require 'org-src)

; The following configuration make the buffer that is opened with =org-edit-src-block= to be opened in the current buffer.

(setq org-src-window-setup 'current-window)

; The following commands map a language to given mode.
;
; These comamnds allow Org Mode
;
; + to assign a given mode when editing a =#+BEGIN_SRC= block by executing =org-edit-src-block=.

(add-to-list 'org-src-lang-modes '("zsh" . sh))
(add-to-list 'org-src-lang-modes '("dash" . sh))


;;; ox-latex

(require 'ox-latex)

; Consider that this compilation process will be used for any document which is exported as a PDF. This is not what you might want since not all of your documents might contain a bibliography. If that's the casse, you are compiling your document twice when only doing it once is necessary.
; biber version 2.14 doesn't require the user provide a value for the flag "--output-directory" because the output directory is obtained from the file path.
; For some reason, using "%O" instead of "%o" produces the following error: I can't write on file "{filename}.log"
; Using the =-shell-escape= flag is necessary because I use the =minted= package instead of the =listings= package for typesetting code snippets. Beware that this might have some bad side effects. For example, compiling a LateX document with =\write18{rm -rf "$RHOME"}= in its content will delete all your files. Use this flag  if you will not compile an arbitrary LaTeX file downloaded from the Internet.

(setq org-latex-pdf-process
      '("%latex -shell-escape -interaction nonstopmode -output-directory %o %b"
        "biber %b"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o %b"))

; The following configuration make the table of contents not to be included in the resulting LaTeX file.
;
; If needed, you can include =#+LATEX_HEADER: \tableofcontents= in your Org file.

(setq org-latex-toc-command "")

; The following configuration make language of code blocks to be mapped to a language that minted can syntax highlight.

(add-to-list 'org-latex-minted-langs '(dash "shell"))

; I don't want hyperref options to be always exported in the preamble because I don't usually use the =hyperref= package.

(setq org-latex-hyperref-template "")

; I prefer using the =minted= package instead of the =listings= package or the =verbatim= environment.
; This has a major drawback: Whenever a document is compiled, =python= is executed which highly increases the compilation time of documents.
; I had previously set =org-latex-default-packages-alist= to empty, so in every document I need to explicitly add a LaTeX header that loads the minted environment

(setq org-latex-listings 'minted)

;;; ob-core

; I don't want to be prompted for confirmation when evaluating code blocks.

(setq org-confirm-babel-evaluate nil)

; The following hook redisplays images whenever a =#+BEGIN_SRC= block has been executed.
;
; This is done because this way I can preview the results of code blocks whose language is LaTeX, Gnuplot.

(require 'org)

(add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

; I prefer results to be always enclosed in "#+begin_example" and "#+end_example" code blocks

(setq org-babel-min-lines-for-block-output 0)

;;;; General header arguments

; Set default header arguments for all code blocks

; =:padline no= disables the insertino of a newline character between two consecutive code blocks which are tangled to the same file.

(setq org-babel-default-header-args
      `((:results . "replace output")
        (:dir . ,(in-experiments))
        (:mkdirp . "yes")
        (:padline . "no")))

;;;; Language-specific header arguments

; =:session none= is required because, otherwise, Emacs freezes after evaluating Python code blocks.

(setq org-babel-default-header-args:python
      `((:tangle . ,(in-experiments "main.py"))
        (:session . "none")))

(setq org-babel-default-header-args:shell
      `((:tangle . ,(in-experiments "shell.sh"))))

(setq org-babel-default-header-args:gnuplot
      `((:tangle . ,(in-experiments "main.gp"))))

(setq org-babel-default-header-args:json
      `((:tangle . ,(in-experiments "main.json"))))

(setq org-babel-default-header-args:yaml
      `((:tangle . ,(in-experiments "main.yaml"))))

(setq org-babel-default-header-args:sh
      `((:tangle . ,(in-experiments "sh.sh"))))

; Sometimes =main.txt= files are tangled for demostration purposes.

(setq org-babel-default-header-args:text
      `((:tangle . ,(in-experiments "main.txt"))))

(setq org-babel-default-header-args:dash
      `((:tangle . ,(in-experiments "dash.sh"))))

(setq org-babel-default-header-args:zsh
      `((:tangle . ,(in-experiments "zsh.sh"))))

(setq org-babel-default-header-args:bash
      `((:tangle . ,(in-experiments "bash.sh"))))

; =:main no= is set because most of the C++ code blocks I write are examples of using different libraries and =#include= statements can't be located within the =int main()= function.

(setq org-babel-default-header-args:cpp
      `((:tangle . ,(in-experiments "main.cpp"))
        (:main . "no")))

(setq org-babel-default-header-args:C
      `((:tangle . ,(in-experiments "main.c"))
        (:main . "no")))

(setq org-babel-default-header-args:javascript
      `((:tangle . ,(in-experiments "main.js"))))

(setq org-babel-default-header-args:html
      `((:tangle . ,(in-experiments "index.html"))))

(setq org-babel-default-header-args:maxima
      `((:tangle . ,(in-experiments "main.mac"))))

; Results must be written in LaTeX in order for them to be typeset in LaTeX when exporting the file.
; Most of the time, I don't want the source code to be exported.

(setq org-babel-default-header-args:latex
      `((:tangle . ,(in-experiments "main.tex"))
        (:results . "latex")
        (:exports . "results")))

(setq org-babel-default-header-args:bibtex
      `((:tangle . ,(in-experiments "references.bib"))))

;;; ol

(require 'ol)

; The following configuration makes links not to be briefly displayed.

(setq org-link-descriptive nil)

(require 'org)

; The following mapping toggles the brief displaying of links.

(define-key org-mode-map (kbd "M-t l") 'org-toggle-link-display)

; The following configuration allow links of the form =[[man:printf(3)]]= to be opened.

(defun org-man-open (link)
  (let ((Man-notify-method 'pushy))
    (man link)))

(org-link-set-parameters "man"
                         :follow 'org-man-open)

(defun org-video-open (name)
  (string-match "\\(.*\\)::\\(.*\\)" name)
  (let* ((player "mpv")
         (file (match-string 1 name))
         (start (match-string 2 name)))
    (start-process player nil player (concat "--start=" start) "--osd-level=3" file)))

(org-link-set-parameters "video"
                         :follow 'org-video-open)

;;; pdf-tools

; The following configuration sets the usage of =pdf-view-mode= instead of =doc-view-mode= when visiting PDF files.
;
; Note that for this to work, the binary =epdfinfo= needs to be installed.

(pdf-loader-install)

;;; reftex-vars

(require 'reftex-vars)

; The following configurations tells reftex which bib files must be taken into consideration.
;
; This make those bib files to be used in any buffer.

(setq reftex-default-bibliography (directory-files "~/my/bib" t "\\.*\\.bib$" t))

;;; reftex-cite

(require 'reftex-cite)

(global-set-key (kbd "C-. c") 'reftex-citation)

;;; yasnippet

(require 'yasnippet)

; This is the directory where I store my own-defined snippets.

(setq yas-snippet-dirs '("~/.emacs.d/snippets"))

; The following global mode make yasnippet's snippets be enabled in all moodes.
;
; I've done this because I use snipppets in multiple modes c++, latex. I could have added those specific modes where I use snippets but that would require updating that list whenever I want to start using snippets in a new mode.

(yas-global-mode 1)

;;; tab-bar

(require 'tab-bar)

;;;; Options

; The following configuration selects when creating a new tab

(setq tab-bar-new-tab-choice "*scratch*")

; The following configuration shows the tab index at the right end of each tab.
;
; By looking at the tab index, I can easily switch to those tabs by presssing =C-M-{index}=.

(setq tab-bar-tab-hints t)

; The following configuration hides the buttons which are present
; + at the right end of each tab that is used to close the corresponding tab.
; + at the right end of the last tab nad is used to create new tabs.

(setq
 tab-bar-close-button-show nil
 tab-bar-new-button-show nil)

;;;; Mappings

; Switch to the last visited tab.

(global-set-key (kbd "<M-tab>") 'tab-bar-switch-to-recent-tab)

; Switch to the next tab.

(global-set-key (kbd "C-x t n") 'tab-bar-switch-to-next-tab)

; Switch to the previous tab.

(global-set-key (kbd "C-x t p") 'tab-bar-switch-to-prev-tab)

; Switch to the tab whose index is the pressed number.

(global-set-key (kbd "C-M-0") (lambda () (interactive) (tab-bar-select-tab 10)))
(global-set-key (kbd "C-M-1") (lambda () (interactive) (tab-bar-select-tab 1)))
(global-set-key (kbd "C-M-2") (lambda () (interactive) (tab-bar-select-tab 2)))
(global-set-key (kbd "C-M-3") (lambda () (interactive) (tab-bar-select-tab 3)))
(global-set-key (kbd "C-M-4") (lambda () (interactive) (tab-bar-select-tab 4)))
(global-set-key (kbd "C-M-5") (lambda () (interactive) (tab-bar-select-tab 5)))
(global-set-key (kbd "C-M-6") (lambda () (interactive) (tab-bar-select-tab 6)))
(global-set-key (kbd "C-M-7") (lambda () (interactive) (tab-bar-select-tab 7)))
(global-set-key (kbd "C-M-8") (lambda () (interactive) (tab-bar-select-tab 8)))
(global-set-key (kbd "C-M-9") (lambda () (interactive) (tab-bar-select-tab 9)))

;;; ace-window

(global-set-key (kbd "M-o") 'ace-window)

; The following configuration makes ace-window not to change the background of any framee when waiting for an input command after =ace-windows= has been executed.

(setq aw-background nil)

; The following configuration set the scope for the current frame. That is, only windows for the current frame will be taken into consideration.

(setq aw-scope 'frame)

; The following mapping make =ace-window= work within =term=.
; 
; TODO Find out why I should use "term-raw-map" over "term-mode-map" in this scenario. The idea of mapping the key in term-raw-map was extracted  from this question at Emacs Stack Exchange (https://emacs.stackexchange.com/questions/37093/how-to-remap-shift-arrow-in-term-mode)

(require 'term)

(define-key term-raw-map (kbd "M-o") 'ace-window)

; The following configuration defines the actions that are to be executed after =ace-windows= has been executed.
;
; The third argument is the text that is shown in the status line. If present, the number of a buffer  is waited to be inserted. Neither =aw-flip-window= nor =delete-other-windows= must contain a third argument. Otherwise, the command is not instantly executed.
; The difference between =aw-switch-buffer-in-window= and =aw-switch-buffer-other-window= is that the former visit the windows while the latter doesn't.

(setq aw-dispatch-alist
  '((?x aw-delete-window "Delete selected window")
    (?m aw-swap-window "Swap windows")
    (?s aw-switch-buffer-in-window "Open buffer in selected window and visita")
	  (?S aw-switch-buffer-other-window "Open buffer in selected window without visiting")
    (?f aw-flip-window)
    (?o delete-other-windows)
    (?O delete-other-windows "Delete other windows")
    (?? aw-show-dispatch-help)))

;;; helm-comint

; This is very intrusive. I don't want to replace defaults
; (global-set-key (kbd "M-x") 'helm-M-x)

(define-key shell-mode-map (kbd "C-c h") #'helm-comint-input-ring)

;;; helm-occur

(require 'helm-occur)

(global-set-key (kbd "<f2> s") 'helm-occur)

;;; helm-elisp

(require 'helm-elisp)

(global-set-key (kbd "<f2> a") 'helm-apropos)

;;; helm-command

(require 'helm-command)

(global-set-key (kbd "<f2> M-x") 'helm-M-x)

;;; helm-bookmark

(require 'helm-bookmark)

(global-set-key (kbd "<f2> b") 'helm-bookmarks)

;;; helm-find

(require 'helm-find)

(global-set-key (kbd "<f2> f") (lambda () (interactive) (helm-find 'ask-for-dir)))
(global-set-key (kbd "<f2> o") (lambda () (interactive) (helm-find-1 "~/my/org")))

;;; helm-buffers

(require 'helm-buffers)

; I preffer =hwlm-buffer-list= over =ibuffer= and =list-buffers=.

(global-set-key (kbd "C-x C-b") 'helm-buffers-list)

;;; helm-org

; The following mapping allows me to search for a heading using helm.
;
; Some of my Org files contain more than 100 headings and subheadings and sometimes searching for an specific heading is difficult because of the existing hierarchies. This mapping solves this issue because I don't have to move the cursor to search for an specific heading since inserting a set of keywords (after the mapping has been triggered) shows me all those headings that contain the input keywords.

(define-key org-mode-map (kbd "C-,") 'helm-org-in-buffer-headings)

;;; org-journal

; Files to be created

(setq org-journal-dir (concat (getenv "my__files") "/journal"))

; File name format for journal files

(setq org-journal-file-format "%Y-%m-%d")

;;; avy

; The following configuration makes =avy= considers the current window.
;
; Most of the times I jump to lines in the current window. For this reason, I don't need avy create jump placeholders in all windows.

(setq avy-all-windows nil)

; While using the home row keys ensures faster keypressings, I've decided to use the number row in order to get used to those keys since these are the only keys which I sometimes see when typing.

(setq avy-keys (number-sequence ?1 ?9))

;;; hydra

; Use of the avy package

(global-set-key (kbd "C-. a")
  (defhydra hydra-avy (:exit t :hint nil)
    "
   Line^^       Region^^        Goto
  ----------------------------------------------------------
   [_y_] yank   [_Y_] yank      [_c_] timed char  [_C_] char
   [_m_] move   [_M_] move      [_w_] word        [_W_] any word
   [_k_] kill   [_K_] kill      [_l_] line        [_L_] end of line"
    ("c" avy-goto-char-timer)
    ("C" avy-goto-char)
    ("w" avy-goto-word-1)
    ("W" avy-goto-word-0)
    ("l" avy-goto-line)
    ("L" avy-goto-end-of-line)
    ("m" avy-move-line)
    ("M" avy-move-region)
    ("k" avy-kill-whole-line)
    ("K" avy-kill-region)
    ("y" avy-copy-line)
    ("Y" avy-copy-region)))

; Choose among multiple grep variants
; Requiring grep is necessary. Otherwise, the first use will not use the grep configuration that was set wtihin the =let= body.

(require 'grep)

(global-set-key (kbd "C-. g")
                (defhydra hydra-grep-variants (:color blue)
"
_r_: Search recursively
_co_: Search configurations (*.org)
_ce_: Search configurations (*.el)
_cm_: Search configurations (*.org, *.el)
"
                   ("r" (let ((grep-host-defaults-alist nil)
                              (grep-command "grep --color -RHIn '' ."))
                          (call-interactively 'grep)))
                   ("co" (let ((grep-host-defaults-alist nil)
                               (grep-command (concat "grep --color -RHIn --include='*.org' '' " path/configs)))
                           (call-interactively 'grep)))
                   ("ce" (let ((grep-host-defaults-alist nil)
                               (grep-find-command (concat "find " path/configs " -type f -name '*.el' ! -path '*/elpa/*' -exec grep --color -HIn '' {} +")))
                           (call-interactively 'find-grep)))
                   ("cm" (let ((grep-host-defaults-alist nil)
                               (grep-find-command (concat "find " path/configs " -type f \\( -name '*.org' -o -name '*.el' \\) ! -path '*/elpa/*' -exec grep --color -HIn '' {} +")))
                           (call-interactively 'find-grep)))))

; Choose among outline mode

(global-set-key (kbd "C-. o")
                (defhydra hydra-outline-mode (:color red)
"
Hide^^                        Show^^                        Movement keys
[_hb_] outline-hide-body      [_sa_] outline-show-all       [_p_] previous-line
[_he_] outline-hide-entry     [_se_] outline-show-entry     [_n_] next-line
[_ht_] outline-hide-subtree   [_ss_] outline-show-subtreee  [_g_] avy-goto-line
[_hl_] outline-hide-leaves    [_sc_] outline-show-children  [_G_] goto-line
[_ho_] outline-hide-other     [_sb_] outline-show-branches  
[_hs_] outline-hide-sublevels [_sh_] outline-show-heading   
"
                  ("p" (previous-line))
                  ("n" (next-line))
                  ("g" (avy-goto-line))
                  ("G" (call-interactively 'goto-line))
                  ("sa" (outline-show-all))
                  ("sb" (outline-show-branches))
                  ("sc" (outline-show-children))
                  ("se" (outline-show-entry))
                  ("sh" (outline-show-heading))
                  ("ss" (outline-show-subtree))
                  ("hb" (outline-hide-body))
                  ("he" (outline-hide-entry))
                  ("hl" (outline-hide-leaves))
                  ("ho" (outline-hide-other))
                  ("ht" (outline-hide-subtree))
                  ("hs" (call-interactively 'outline-hide-sublevels))))


; Organize tabs

(global-set-key (kbd "C-. t")
                (defhydra hydra-tab-management (:color red)
"
Miscelanous^^            Switch^^                         Move
[_n_] Create new tab     [_h_] Switch to the previous tab [_M-h_] Backwardly move current tab
[_r_] Rename current tab [_l_] Switch to the next tab     [_M-l_] Forwardly move current tab
[_x_] Remove current tab
"
                  ("r" (call-interactively 'tab-bar-rename-tab))
                  ("h" (call-interactively 'tab-bar-switch-to-prev-tab))
                  ("l" (call-interactively 'tab-bar-switch-to-next-tab))
                  ("n" (tab-bar-new-tab))
                  ("x" (tab-bar-close-tab))
                  ("M-h" (lambda () (interactive) (tab-bar-move-tab -1)))
                  ("M-l" (lambda () (interactive) (tab-bar-move-tab 1)))))

; Purge music

(global-set-key (kbd "C-. m")
                (defhydra hydra-mpc (:color red)
                  ("n" (start-process "mpc" nil "mpc" "next"))
                  ("p" (start-process "mpc" nil "mpc" "prev"))
                  ("s" (start-process "mpc" nil (concat (getenv "my__scripts") "/music_player/save_cur_song")))
                  ("d" (start-process "mpc" nil (concat (getenv "my__scripts") "/music_player/del_cur_song")))
                  ("0" (start-process "mpc" nil "mpc" "seek" "0%"))
                  ("1" (start-process "mpc" nil "mpc" "seek" "10%"))
                  ("2" (start-process "mpc" nil "mpc" "seek" "20%"))
                  ("3" (start-process "mpc" nil "mpc" "seek" "30%"))
                  ("4" (start-process "mpc" nil "mpc" "seek" "40%"))
                  ("5" (start-process "mpc" nil "mpc" "seek" "50%"))
                  ("6" (start-process "mpc" nil "mpc" "seek" "60%"))
                  ("7" (start-process "mpc" nil "mpc" "seek" "70%"))
                  ("8" (start-process "mpc" nil "mpc" "seek" "80%"))
                  ("9" (start-process "mpc" nil "mpc" "seek" "90%"))))

;; Auto configuration

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("~/Experiments/main.org" "~/my/org/Algorithms and data structures/Binary search tree.org" "~/my/org/Algorithms and data structures/Disjoint sets.org" "~/my/org/Algorithms and data structures/Heap.org" "~/my/org/Algorithms and data structures/MIT.org" "~/my/org/Algorithms and data structures/Singly linked list.org" "~/my/org/Algorithms and data structures/main.org" "~/my/org/Commands/7z.org" "~/my/org/Commands/cmus.org" "~/my/org/Commands/ffmpeg.org" "~/my/org/Commands/ffprobe.org" "~/my/org/Commands/find.org" "~/my/org/Commands/firefox.org" "~/my/org/Commands/git.org" "~/my/org/Commands/grep.org" "~/my/org/Commands/man.org" "~/my/org/Commands/mpv.org" "~/my/org/Commands/ncmpcpp.org" "~/my/org/Commands/pacman.org" "~/my/org/Commands/rsync.org" "~/my/org/Commands/tags.org" "~/my/org/Commands/tar.org" "~/my/org/Commands/wget.org" "~/my/org/Commands/youtube-dl.org" "~/my/org/Commands/zathura.org" "~/my/org/Mathematics/Discrete mathematics.org" "~/my/org/Mathematics/Precalculus.org" "~/my/org/Mathematics/Trigonometry.org" "~/my/org/Programming languages/C++/Library - Boost.org" "~/my/org/Programming languages/C++/main.org" "~/my/org/Programming languages/AWK.org" "~/my/org/Programming languages/C.org" "~/my/org/Programming languages/Elisp.org" "~/my/org/Programming languages/JSON.org" "~/my/org/Programming languages/Javascript.org" "~/my/org/Programming languages/LaTeX.org" "~/my/org/Programming languages/Maxima.org" "~/my/org/Programming languages/Python.org" "~/my/org/Programming languages/gnuplot.org" "~/my/org/Programming languages/makefile.org" "~/my/org/Programming languages/sed.org" "~/my/org/Books.org" "~/my/org/Doxygen.org" "~/my/org/Text processing.org" "~/my/org/Web scraping.org" "~/my/org/compression.org" "~/my/org/dotfiles.org" "~/my/org/gcc.org" "~/my/org/gdb.org" "~/my/org/golf.org" "~/my/org/mount.org" "~/my/org/org.org" "~/my/org/pdf.org" "~/my/org/permissions.org" "~/my/org/search.org" "~/my/org/shell.org" "~/my/org/stackexchange.org"))
 '(package-selected-packages
   '(htmlize toc-org maxima gnuplot pdf-tools helm-org org-journal hydra helm ace-window yasnippet adaptive-wrap)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )



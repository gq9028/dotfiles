# * Miscelanous

# Make disabled commands not to ask for confirmation from the user when they are used.

# More information on disabled commands in ~(emacs) Disabling~.

# #+begin_src emacs-lisp
# (put 'erase-buffer 'disabled nil)
# (put 'upcase-region 'disabled nil)
# (put 'downcase-region 'disabled nil)
# (put 'set-goal-column 'disabled nil)
# #+end_src

# #+begin_src emacs-lisp
# (setq backup-directory-alist `(("." . "~/.emacs.d/backup")))
# #+end_src

# * Appearance

# ** GUI

# #+begin_src emacs-lisp
# (tool-bar-mode -1)
# (menu-bar-mode -1)
# (toggle-scroll-bar -1)
# #+end_src

# * Bindings

# #+begin_src emacs-lisp
# (global-set-key (kbd "C-x C-b") 'ibuffer)
# (global-set-key (kbd "C-s") 'isearch-forward-regexp)
# (global-set-key (kbd "C-r") 'isearch-backward-regexp)
# #+end_src

# * Packages

# ** ~org~

# #+begin_src emacs-lisp
# (setq org-agenda-files
#   (directory-files "~/my/org" 1 "^[^\.].*\\.org$" 1))
# #+end_src

# #+begin_src emacs-lisp
# (setq org-todo-keywords '(
#   (sequence "TODO(y@)" "DOING(h)" "|" "DONE(n@)")
#   (sequence "IMPROVE(u@)" "IMPROVING(j)" "|" "IMPROVED(m@)")
#   (sequence "WAITING(i@)" "|" "CANCELLED(k@)")))
# #+end_src

# #+begin_src emacs-lisp
# (setq org-time-stamp-formats '("<%Y-%m-%d %a>" . "<%Y-%m-%d %a %H:%M:%S>"))
# #+end_src

# ** ~org-journal~

# #+begin_src emacs-lisp
# (setq org-journal-dir "~/my/journal/")
# (setq org-journal-file-format "%Y_%m_%d_%a")
# (setq org-journal-date-format "%d/%m/%Y (%a)")
# (setq org-journal-time-format "")
# (require 'org-journal)
# #+end_src

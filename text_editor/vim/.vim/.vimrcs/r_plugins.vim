call plug#begin('~/.vim/plugged')

" General {{{1

call junegunn#fzf#main()

Plug 'tpope/vim-commentary'
Plug 'vim-utils/vim-man'

" SirVer/ultisnips {{{2

Plug 'SirVer/ultisnips'

" General 

let g:UltiSnipsSnippetDirectories=[$my__vim_snippets]

" Keybindings 

nnoremap <Leader>usrs :call UltiSnips#RefreshSnippets()<CR>
nnoremap <Leader>usls :call UltiSnips#ListSnippets()<CR>

let g:UltiSnipsExpandTrigger='<tab>'

" vim-scripts/loremipsum {{{2

Plug 'vim-scripts/loremipsum'

nnoremap <Leader>l :Loremipsum 

" neoclide/coc.nvim {{{2

Plug 'neoclide/coc.nvim', {'branch': 'release'}

let g:coc_global_extensions = [
		\ 'coc-json',
		\ 'coc-python',
\ ]

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" nmap <silent> <Leader>cdi :CocList diagnostics<CR>
" nmap <silent> [g <Plug>(coc-diagnostic-next)
" nmap <silent> ]g <Plug>(coc-diagnostic-prev)

" machakann/vim-sandwich {{{2

Plug 'machakann/vim-sandwich'

let g:textobj_sandwich_no_default_key_mappings = 1

for s:char in [ '_', '.', ':', ',', ';', '<bar>', '/', '<bslash>', '*', '+', '%', '$' ]
  execute 'xmap i' . s:char . ' <Plug>(textobj-sandwich-query-i)' . s:char
  execute 'omap i' . s:char . ' <Plug>(textobj-sandwich-query-i)' . s:char
  execute 'xmap a' . s:char . ' <Plug>(textobj-sandwich-query-a)' . s:char
  execute 'omap a' . s:char . ' <Plug>(textobj-sandwich-query-a)' . s:char
endfor

" Filetype-dependent {{{1 

" lervag/vimtex {{{2

Plug 'lervag/vimtex' , {
	\ 'for' : [
		\ 'tex'
	\ ]
\ }

" misc

let g:vimtex_indent_enabled=0

" folding

let g:vimtex_fold_enabled = 1

let g:vimtex_fold_types = {
	\ 'cmd_single' : {'enabled' : 0},
	\ 'cmd_single_opt' : {'enabled' : 0},
	\ 'cmd_multi' : {'enabled' : 0},
	\ 'comments' : {'enabled' : 0},
	\ 'env_options' : {'enabled' : 0},
	\ 'envs' : {
	\ 	'whitelist' : ['frame', 'solution', 'column']
	\ },
	\ 'sections' : {
	\   'sections' : [
	\     'chapter',
	\     'section',
	\     'subsection',
	\     'subsubsection',
	\   ],
	\ }
\}

nnoremap <leader>fv :setlocal
			\ foldexpr=vimtex#fold#level(v:lnum)
			\ foldtext=vimtex#fold#text()<CR>

" Mappings

let g:vimtex_imaps_list = []

augroup lervag#vimtex#map
	au!
	au filetype tex
				\ xmap <buffer> im <Plug>(vimtex-i$)
				\| omap <buffer> im <Plug>(vimtex-i$)
				\| xmap <buffer> am <Plug>(vimtex-a$)
				\| omap <buffer> am <Plug>(vimtex-a$)
augroup end

" mattn/emmet-vim {{{2

Plug 'mattn/emmet-vim', {
	\ 'for' : [
		\ 'css',
		\ 'sass',
		\ 'scss',
		\ 'less',
		\ 'html',
		\ 'elm',
		\ 'xml',
		\ 'htmldjango',
		\ 'jade',
		\ 'pug',
		\ 'xsl',
		\ 'jsx',
		\ 'tsx',
		\ 'xslt',
		\ 'haml',
		\ 'slim',
		\ 'xhtml',
		\ 'mustache',
		\ 'xsd'
	\ ] }

" dense-analysis/ale {{{2

Plug 'dense-analysis/ale' , {
	\ 'for' : [
		\ 'python',
		\ 'cpp',
		\ 'sh',
	\ ]
\ }

" fix

let g:ale_fixers = {
	\ 'python': ['autopep8'],
	\ 'cpp': ['clang-format']
\}

let g:ale_fix_on_save = 0

" lint 

let g:ale_linters = {
	\ 'python': ['flake8'],
	\ 'cpp': ['g++'],
	\ 'sh': ['shellcheck'],
	\}

let g:ale_open_list = 0
let g:ale_set_quickfix = 1

" mappings 

nnoremap ]g :ALENext<cr>
nnoremap [g :ALEPrevious<cr>
nnoremap <leader>af :ALEFix<cr>
nnoremap <leader>at :ALEToggle<cr>

" masukomi/vim-markdown-folding {{{2

Plug 'masukomi/vim-markdown-folding' , {
	\ 'for' : [
		\ 'markdown'
	\ ]
\ }

" Unused {{{1

" call ycmcore#YouCompleteMe#main()
" call preservim#nerdtree#main()
" call prettier#vimprettier#main()
" call jiangmiao#autopairs#main()

" }}}1

call plug#end()

" Miscelanous {{{1

" Settings {{{2

set nocp
set dir=.
set lz
set enc=utf-8
set bs=indent,eol,start
set splitright
let g:tex_flavor="latex"

" Leader key

let mapleader = "\\"

" Swap

set dir=$my__vim/swap//

" Cursor

set ve=onemore

" Splitting

set sb

" Autocommands {{{2

au
			\ BufWritePost
			\ $HOME/.config/dunst/dunstrc
			\ call system("killall dunst ; eval 'dunst &' && disown")

au
			\ BufWritePost
			\ $HOME/.config/sxhkd/sxhkdrc
			\ call system("killall sxhkd ; eval 'sxhkd &' && disown")

" Mappings {{{2

nnoremap <c-_><c-_> :wa<cr>
nnoremap <leader>ca :% y+<cr>
nnoremap <leader>cl :let @+ = getline(".")<cr>
nnoremap <leader>cp :let @+ = expand("%:p")<cr>
nnoremap <leader>da :% d_<cr>
nnoremap <leader>ev :sp $my__vim/.vimrcs/r_vanilla.vim<cr>
nnoremap <leader>rs :exe ":-1read $my__skeletons/" . &filetype <cr>
nnoremap <leader>rs :exe ":-1read $my__skeletons/" . &filetype <cr>
nnoremap <leader>rw :%s,\v<>,,g<left><left><left><left>
nnoremap <leader>sa GVggzz
nnoremap <leader>cd :cd %:p:h<cr>
nnoremap <c-w>gv :vs <cfile><cr>

xnoremap <leader>rw :s,\v<>,,g<left><left><left><left>
xnoremap <leader>r :s,

" Write, close & quit

nnoremap <c-_>w :w<cr>
nnoremap <c-_>wa :wa<cr>
nnoremap <c-_>c :bd<cr>
nnoremap <c-_>q :qa!<cr>

" Quickfix window

nnoremap <c-_>, :cp<cr>
nnoremap <c-_>. :cn<cr>

" }}}
" Commands {{{2

fun! SocialSnippets(A, L, P)
	let l:files = split(globpath('$my__vim_snippets', '*_social.snippets'), '\n')
	for i in range(0, len(l:files)-1)
		let l:files[i] = substitute(fnamemodify(l:files[i], ":t:r"), "_social", "", "")
	endfor
	return l:files
endfun

nnoremap <leader>sn :call feedkeys(":Social \<Tab>", 't')<cr>
command! -complete=customlist,SocialSnippets -nargs=1 Social :setf <args>

fun! FuncReverseLines(LNumStart, LNumEnd)
	silent execute a:LNumStart . ',' . a:LNumEnd  . "g:^:m" .  (a:LNumStart == 1 ? '' : (a:LNumStart - 1))
endfun

command! -range=% ReverseLines call FuncReverseLines(<line1>, <line2>)

" If 'reg' is a lowercase letter then the register whose identifier is 'reg' will be cleared and all matching patterns will be saved into that register.
" If 'reg' is an uppercase letter then the register whose identifier is 'reg' will not be cleared and all matching patterns will be appended into that register.
fun! CopyMatches(reg)
  let hits = []
  %s##\=len(add(hits, submatch(0))) ? submatch(0) : '' #gne
	if(char2nr(a:reg) <= 122 && char2nr(a:reg) >= 97)
		execute 'let @' . a:reg . ' = ""'
		execute 'let @' . a:reg . ' = join(hits, "\n")'
	elseif(char2nr(a:reg) <= 90 && char2nr(a:reg) >= 65)
		execute 'let @' . tolower(a:reg) . ' .= "\n"'
		execute 'let @' . tolower(a:reg) . ' .= join(hits, "\n")'
	endif
endfun

command! -register CopyMatches call CopyMatches(<q-reg>)

fun! FuncSortByLength(LNumStart, LNumEnd, IsReverse)
	silent execute a:LNumStart . ',' . a:LNumEnd . 's#\v^#\=strchars(getline(".")) . " "'
	silent execute a:LNumStart . ',' . a:LNumEnd . 'sort' . (a:IsReverse ? '!' : '') . ' n'
	silent execute a:LNumStart . ',' . a:LNumEnd . 's#\v\d+ #'
endfun

command! -bang -range=% SortByLength call FuncSortByLength(<line1>, <line2>, <bang>0)

" Functions {{{2

fun! CharDecimalValue(lowerb, upperb)
	for i in range(a:lowerb, a:upperb)
		let l:si = string(i)
		while len(l:si) < 3
			let l:si = '0' . l:si
		endwhile
		exe "normal i\<c-v>" . l:si . " " . l:si . " " . printf("%x", l:si) . " " . "\<c-j>"
	endfor
endfun

fun! MainMake()
	let l:make_target = (exists("b:make_target")
				\? b:make_target
				\: "mymain")

	call system(
				\'tmux new-window '
				\. ' "make -f '
				\. $my__makefiles
				\. '/'
				\. &filetype
				\. ' '
				\. l:make_target
				\. ' 2>&1 | tee tmp.log"')
endfun

fun! ToggleIfScope()
	if ! exists("b:if_block_start")
		echoe 'The "b:if_block_start" variable is not set.'
		return
	en

	let l:line_nr = search('\v^' . b:if_block_start . '$', 'bn')

	if l:line_nr == 0
		return
	en

	normal m`

	if search('\v^' . b:if_true . '$', 'wn') != 0
		exe '%s,' . b:if_true . ',' . b:if_false
		normal ``
	en

	exe l:line_nr . 's,' . b:if_false . ',' .b:if_true
	normal ``
endfun

nnoremap <c-c>t :call ToggleIfScope()<cr>

" Auto closing openers {{{2
" It doesn't write the closing openers if it's already present

" TODO: Set this settings on a file which is sourced after vim knows the filetype of the file.
"if (index(['tex'], &filetype) != -1)
"	inoremap <buffer>        {  {}<left>
"	inoremap <buffer> <expr> }  strpart(getline('.'), col('.')-1, 1) == "}" ? "\<Right>" : "}"
"endif
"
"if (index(['tex'], &filetype) == -1)
"	inoremap <buffer>        (  ()<left>
"	inoremap <buffer> <expr> )  strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"
"endif
"
"if (index(['tex'], &filetype) == -1)
"	inoremap <buffer>        [  []<left>
"	inoremap <buffer> <expr> ]  strpart(getline('.'), col('.')-1, 1) == "]" ? "\<Right>" : "]"
"endif

" It doesn't make digraphs (accented letters 'a, 'e, 'i, 'o, 'u) to work correctly.
" The reason why I don't use <c-k> to write digraphs is because that key is used by UltiSnips.
"inoremap <expr> '  strpart(getline('.'), col('.')-1, 1) == "'" ? "\<Right>" : "\'\'\<left>"
"inoremap '' '

"inoremap <expr> "  strpart(getline("."), col(".")-1, 1) == "\"" ? "\<Right>" : "\"\"\<left>"
"inoremap "" "

" }}}1

" Auto pairs {{{1

inoremap [ []<left>
inoremap [[ [
inoremap <expr> ] getline('.')[col('.')-1] == "]" ? "\<right>" : "]"

inoremap ( ()<left>
inoremap (( (
inoremap <expr> ) getline('.')[col('.')-1] == ")" ? "\<right>" : ")"

inoremap { {}<left>
inoremap {{ {
inoremap <expr> } getline('.')[col('.')-1] == "}" ? "\<right>" : "}"
inoremap {<c-j> {<c-j>}<esc>O

inoremap <expr> ' getline('.')[col('.')-1] == "'" ? "\<right>" : "''\<left>"
inoremap '' '

inoremap <expr> " getline('.')[col('.')-1] == "\"" ? "\<right>" : "\"\"\<left>"
inoremap "" "

" Command line {{{1

set wmnu

cnoremap <c-p> <Up>

function! SmartEnter()
	let cmdline = getcmdline()
	if cmdline =~ '\v\C/(#|nu%[mber])$'
		return "\<c-j>:"
	elseif cmdline =~ '\v\C^(ls|files|buffers)'
		return "\<c-j>:e #"
	elseif cmdline =~ '\v\C^ol%[dfiles]'
		return "\<c-j>:e #<"
	elseif cmdline =~ '\v\C^tabs'
		return "\<c-j>:tabn "
	else
		return "\<c-j>"
	endif
endfunction

cnoremap <expr> <c-j> SmartEnter()

" }}}
" Bad habits ripper {{{1

" SOL: <c-h>
noremap! <bs> <nop>

" " SOL: <c-j>
" noremap! <cr> <nop>

" SOL: o
nnoremap A<c-j> <nop>

" SOL: O
nnoremap I<c-j> <nop>

" SOL: I
nnoremap 0i <nop>

" SOL: x
nnoremap dl <nop>

" SOL: s
nnoremap cl <nop>

" SOL: $x
nnoremap $D <nop>

" SOL: X
nnoremap hx <nop>

" <c-p> has not been included in this list, because this key has been mapped
" to a function that compiles the document. When I compile the document, I
" don't want to the layout to finish.

for i in [
			\'<c-a>', '<c-x>',
			\'<c-n>',
			\'x', 'X'
			\]
	exe 'nnoremap ' . i . i . ' <nop>'
endfo

for i in ['<down>', '<left>', '<right>', '<up>']
	exe 'noremap! ' . i . ' <nop>'
	exe 'nnoremap ' . i . ' <nop>'
	exe 'xnoremap ' . i . ' <nop>'
endfo

for i in ['<c-h>', '<c-w>']
	exe 'inoremap ' . i . i . ' <nop>'
endfo

for i in [
			\'h', 'j', 'k', 'l',
			\'n', 'N',
			\'w', 'W',
			\'b', 'B',
			\'e', 'E',
			\'{', '}',
			\',', ';',
			\'<c-u>', '<c-d>',
			\'<c-b>', '<c-f>',
			\]
	exe 'nnoremap ' . i . i . ' <nop>'
	exe 'xnoremap ' . i . i . ' <nop>'
endfo

" Completion {{{1

set cpt=.,w,b,k$my__resources/tex/bib/*.bib
set cot=menu,popup
nnoremap <leader>b :tabnew $my__resources/tex/bib<cr>

" Colour scheme {{{1

let g:DarkCS = "ron"
let g:LightCS = "morning"

exec "colorscheme " . g:DarkCS
nnoremap <leader>co :if g:colors_name != g:DarkCS <bar> exec "colorscheme " . g:DarkCS <bar> else <bar> exec "colorscheme " . g:LightCS <bar> endif <cr>

" Digraphs {{{1

set dg
digr
			\ nn 8469
			\ rr 8477
			\ zz 8484

" Text formatting {{{1

" Format

set fo=

" Width

set tw=0

" Indentation

set ts=2
set sw=2
set noet
set ai
set et

" Word wrapping

set wm=0
set wrap
set bri

" Syntax {{{1

syntax off
set synmaxcol=80

" Terminal {{{1

set sh=dash

tnoremap <c-p> <c-w>:bd!<cr>
autocmd TerminalOpen * nnoremap <buffer> <c-p> :bd!<cr>

" Fold {{{1

set fdm=marker
set fdl=0
set fdo=

nnoremap <space> za

" Currently working for programming languages whose conditionals have an
" explicit ending (e.g. "tex", "c" and "cpp" filetypes).
"
" Find out how to make it work with "python" files.
fu! MyNotesFold()
	let line = getline(v:lnum)

	if match(line, '\v^' . b:if_block_start . '$') > -1
		return '>1'
	elseif match(line, '\v^' . b:if_block_end . '$') > -1
		return '<1'
	else
		return '='
	en
endf

nnoremap <leader>fn :setlocal
			\ fdm=expr
			\ fde=MyNotesFold()
			\ fdt=foldtext()<cr>

nnoremap <leader>fm :setlocal
			\ fdm=marker
			\ fdt=foldtext()<cr>

nnoremap <leader>fi :setlocal
			\ fdm=indent
			\ fdt=foldtext()<cr>

" Search {{{1

set nois
set hls
set ic
set scs
set nows

nnoremap / /\v
nnoremap ? ?\v

nnoremap <c-_>n :noh<cr>

" SFR: [s]earch [w]ord
nnoremap <leader>sw /\v<><left>

" SFR: [s]earch using the [g] command
nnoremap <leader>sg :g//#<left><left>

" SFR: [s]earch pattern at the [b]eginning of all lines
nnoremap <leader>sb /\v(^\s*)@<=

" SFR: [f]lag [o]currence
nnoremap <leader>sfo /\C\v-><left>

" SFR: [f]lag [d]escription
nnoremap <leader>sfd /\C\v(^\s*)@<=-><left>

" SFR: [c]ommand [d]escription
nnoremap <leader>scd /\C\v(^\s*)@<=><left>

" SFR: [c]ommand [o]currence
nnoremap <leader>sco /\C\v<><left>


" Windows {{{1

" Movement

nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" Status line {{{1

set ls=2
set stl=[%l][%Y][%t]

" Configuration options togglers {{{1

nnoremap <leader>tc :if &cc == 80 <bar> set cc=0 <bar> else <bar> set cc=80 <bar> endif<cr>
nnoremap <leader>tg :set<Space>

fun! ToggleCursorInformation()
	hi CursorLine ctermbg=8
	hi CursorColumn ctermbg=8

	set cursorline! cursorcolumn!
endfun

nnoremap <leader>ti :call ToggleCursorInformation()<cr>
nnoremap <leader>tl :setlocal<Space>
nnoremap <leader>tn :windo set number!<cr>
nnoremap <leader>tp :set spell!<cr>
nnoremap <leader>ts :set ws!<cr>
nnoremap <leader>ty :if exists("g:syntax_on") <bar> syntax off <bar> else <bar> syntax on <bar> endif<cr>
nnoremap <leader>tw :windo set wrap!<cr>
nnoremap <leader>tz :let &scrolloff=999-&scrolloff<cr>

" Spell {{{1

set spelllang=en,es
set spellfile=$my__vim/spell/en.utf-8.add,$my__vim/spell/es.utf-8.add

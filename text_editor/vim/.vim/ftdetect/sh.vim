au BufNewFile,BufRead
			\ $HOME/.bin/*
			\,$HOME/.aliases
			\,$HOME/.shellrc
			\,$HOME/.functions
			\ set ft=sh

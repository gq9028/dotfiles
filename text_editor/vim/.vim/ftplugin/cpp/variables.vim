let b:if_block_start = '#if [01]'
let b:if_block_end = '#endif'
let b:if_false = '#if 0'
let b:if_true = '#if 1'

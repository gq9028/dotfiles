fun! Compile()
	wa

	call system("tmux set-environment SRC " . shellescape(expand('%:r')))
	call system("tmux set-environment OUT_DIR " . shellescape(expand('%:h')))

	call MainMake()
endfunction

nnoremap <buffer> <c-p> :call Compile()<CR>

fun! PreviewPDF()
	let l:pdf_file = shellescape(expand('%:r') . '.pdf')
	call system('xdg-open ' . l:pdf_file . ' &')
endfunction

nnoremap <buffer> <c-c>p :call PreviewPDF()<CR>

let b:if_block_start = '\\if(true|false)'
let b:if_block_end = '\\fi'
let b:if_false = '\\iffalse'
let b:if_true = '\\iftrue'

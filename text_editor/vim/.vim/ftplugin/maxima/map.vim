fun! ExecuteFile()
	wa
	exe 'term dash -c "cat ' . expand("%") . ' | maxima --very-quiet"'
endfun

nnoremap <silent> <buffer> <c-p> :call ExecuteFile()<CR>

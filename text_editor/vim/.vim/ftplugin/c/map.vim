fun! Compile()
	wa

	let l:src = shellescape(expand('%'))
	let b:envs = '-e SRC=' . l:src

	call MainMake()
endfunction

" nnoremap <buffer> <c-p> :call Compile()<cr>

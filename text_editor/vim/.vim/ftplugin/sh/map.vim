fun! Run()
	wa

	let $SRC = shellescape(expand('%'))
	let $MYSHELL = &shell

	call MainMake()
endfunction

nnoremap <buffer> <c-p> :call Run()<cr>

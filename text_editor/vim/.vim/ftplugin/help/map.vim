nnoremap <buffer> > :cn<CR>
nnoremap <buffer> < :cp<CR>
nnoremap <buffer> A :cla<CR>
nnoremap <buffer> I :cfir<CR>

fun! ConvertToHTML()
	write
	let outputFile = shellescape(expand("%:r") . ".html", 1)
	let sourceFile = shellescape(expand("%:.:p"), 1)

	exec "!pandoc -o " . outputFile . " -s -c ~/.config/pandoc/styles/github.css " . sourceFile . " 2>/dev/null"
endfunction

fun! ConvertToPDF()
	call ConvertToHTML()

	let outputFile = shellescape(expand("%:r") . ".pdf", 1)
	let sourceFile = shellescape(expand("%:r") . ".html", 1)

	silent! exe "!wkhtmltopdf " sourceFile . " " . outputFile
endfunction

fun! Preview(extension)
	let sourceFile = shellescape(expand("%:r") . "." . a:extension, 1)
	exe "!xdg-open " . sourceFile . " 2>/dev/null &"
endfunction

nnoremap <buffer> <F1> :call ConvertToHTML()<CR>
nnoremap <buffer> <F2> :call ConvertToPDF()<CR>
nnoremap <buffer> <F11> :call Preview("html")<CR>
nnoremap <buffer> <F12> :call Preview("pdf")<CR>

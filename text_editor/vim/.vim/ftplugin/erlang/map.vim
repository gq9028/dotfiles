" TODO
" 	* This mapping must work even when the file has been opened as a full
" 	path instead of having opened it in the current working directory.
" 	* It must accept "n" function calls instead of a single one.
" 	* Subsequent commands of the "erlc" command must only be executed if and
" 	only if the "erlc" command was executed succesfully.
fun! CompileAndExecute(...)
	if a:0 == 0 && ! exists("g:LastExecutedFunction")
		return
	endif

	let FileWithoutExtension = expand("%:t:r")
	exe "!erlc " . expand("%")
	new
	if a:0 == 0
		if exists("g:LastExecutedFunction")
			exe "-1r !erl -noshell -s " . FileWithoutExtension . " " . g:LastExecutedFunction . " -s init stop"
		endif
	elseif a:0 == 1
		exe "-1r !erl -noshell -s " . FileWithoutExtension . " " . a:1 " -s init stop"
		let g:LastExecutedFunction = a:1
	endif
endfunction

command! -nargs=? Execute :call CompileAndExecute(<args>)
nnoremap <Leader>ec :Execute 

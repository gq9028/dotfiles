fun! Run()
	wa

	call system("tmux set-environment SRC " . shellescape(expand('%')))

	call MainMake()
endfunction

nnoremap <buffer> <c-p> :call Run()<cr>

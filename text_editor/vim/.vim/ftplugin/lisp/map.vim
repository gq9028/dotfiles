fun! ExecuteFile()
	sil wa

	let l:i = expand("%")

	exe "term sbcl --script " . l:i
endfun

nnoremap <silent> <buffer> <c-p> :call ExecuteFile()<CR>

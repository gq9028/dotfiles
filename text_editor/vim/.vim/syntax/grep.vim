" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

syn match FileName /\v^[^:]+/ nextgroup=Separator1
syn match Separator1 /:/ nextgroup=LineNumber contained
syn match LineNumber /\d*/ nextgroup=Separator1 contained

hi link FileName constant
hi link Separator1 comment
hi link LineNumber type

let b:current_syntax = "grep"

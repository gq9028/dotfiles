fu! junegunn#fzf#main()
	" Configuration options {{{1

	let g:fzf_action = {
		\ 'ctrl-t': 'tab split',
		\ 'ctrl-s': 'split',
		\ 'ctrl-v': 'vsplit' }

	" Keybindings {{{1

	nnoremap <leader>en :call fzf#run(fzf#wrap({
				\'source': 'find $my__notes',
				\'options': '-m -e'
				\}, 1))<cr>

	nnoremap <leader>e. :call fzf#run(fzf#wrap({
				\'source': 'find',
				\'options': '-m -e'
				\}, 1))<cr>

	nnoremap <leader>ec :call fzf#run(fzf#wrap({
				\'dir': '$my__vim',
				\'source': 'find -type l',
				\'options': '-m -e'
				\}, 1))<cr>

	nnoremap <leader>esf :call fzf#run(fzf#wrap({
				\'dir': '$my__vim_snippets' . '/' . &filetype,
				\'source': 'find -type l',
				\'options': '-m -e'
				\}, 1))<cr>

	nnoremap <leader>esa :call fzf#run(fzf#wrap({
				\'dir': '$my__vim_snippets',
				\'source': 'find -type l',
				\'options': '-m -e'
				\}, 1))<cr>

	nnoremap <leader>ef :call fzf#run(fzf#wrap({
				\'dir': '$my__vim' . '/ftplugin/' . &filetype,
				\'source': 'find -type l',
				\'options': '-m -e'
				\}, 1))<cr>

	" }}}1
endf

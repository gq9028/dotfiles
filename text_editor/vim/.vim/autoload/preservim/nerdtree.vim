fu! preservim#nerdtree#main()
	" Installation {{{1
	
	Plug 'preservim/nerdtree'

	" Configuration options {{{1

	let g:NERDTreeMinimalUI = 1
	let g:NERDTreeShowHidden = 1
	let g:NERDTreeWinPos = "left"

	" Keybindings {{{1

	" Default

	let g:NERDTreeMapActivateNode = 'l'

	" Main

	nnoremap <c-n> :NERDTree<cr>
	nnoremap <Leader>oc :exe ":NERDTree " . expand("%:p:h")<CR>

	" Templates

	nnoremap <Leader>ot :exe ":NERDTree " . $my__templates <CR>
	nnoremap <Leader>otf :exe ":NERDTree " . $my__templates . '/' . &filetype<CR>
	nnoremap <Leader>ota :exe ":NERDTree" . $my__templates . '/all'<CR>

	" Snippets

	nnoremap <Leader>os :exe ":NERDTree " . $my__vim_snippets <CR>
	nnoremap <Leader>osf :exe ":NERDTree " . $my__vim_snippets . '/' . &filetype <CR>
	nnoremap <Leader>osa :exe ":NERDTree " . $my__vim_snippets . "/all"<CR>

	" Notes

	nnoremap <Leader>on :exe ":NERDTree " . $my__notes <CR>
	nnoremap <Leader>onf :exe ":NERDTree " . $my__notes . '/' . &filetype <CR>

	" Resources

	nnoremap <Leader>or :exe ":NERDTree " . $my__resources<CR>
	nnoremap <Leader>orf :exe ":NERDTree " . $my__resources . '/' . &filetype <CR>
	" }}}1
endf

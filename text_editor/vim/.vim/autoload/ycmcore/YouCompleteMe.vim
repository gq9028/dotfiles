fu! ycmcore#YouCompleteMe#main()
	" Installation {{{1
	
	Plug 'ycm-core/YouCompleteMe'

	" Configuration options {{{1

	let g:ycm_key_list_select_completion = ['<C-n>']

	" }}}1
endf

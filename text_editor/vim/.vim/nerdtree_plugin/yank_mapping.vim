call NERDTreeAddKeyMap({
        \ 'key': 'pf',
        \ 'callback': 'NERDTreeYankPathFull',
        \ 'quickhelpText': 'Put full path of current node into the default register' })

call NERDTreeAddKeyMap({
        \ 'key': 'pr',
        \ 'callback': 'NERDTreeYankPathRelative',
        \ 'quickhelpText': 'Put relative path of current node into the default register' })

call NERDTreeAddKeyMap({
        \ 'key': 'rf',
        \ 'callback': 'NERDTreeYankContent',
        \ 'quickhelpText': 'Put content of current node into the default register' })

function! NERDTreeYankPathFull()
	let n = g:NERDTreeFileNode.GetSelected()
	call setreg('"', n.path.str() . "\n")
endfunction

function! NERDTreeYankPathRelative()
	let n = g:NERDTreeFileNode.GetSelected()
	call setreg('"', fnamemodify(n.path.str() . "\n", ':.'))
endfunction

function! NERDTreeYankContent()
	let n = g:NERDTreeFileNode.GetSelected()
	call setreg('"', join(readfile(n.path.str()), "\n"))
endfunction

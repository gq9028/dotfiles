" In vim 8.2, the 'indentexpr' configuration option is set in the
" '/usr/share/vim/vim82/indent/tex.vim' file.
setlocal indentexpr=

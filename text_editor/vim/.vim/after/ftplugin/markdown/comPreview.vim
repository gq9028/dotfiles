" In vim 8.2, the "markdown" filetype is also treated as the "html" filetype
" which means that the mappings created for the "markdown" filetype can be
" overwritten by the "html" filetype. Because of this, this mappings are
" sourced again, after the mappings for the "html" filetype has been sourced.

source $my__vim/ftplugin/markdown/map.vim

" Folding settings are set within the UltiSnips plugin folder, which is sourced
" after ~/.vimrc.
set foldmethod=marker
set foldlevel=0

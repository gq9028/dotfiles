#!/bin/bash

packageFolder=$(pwd)
installerSchema=installOn
osIdentifier=gnulinux

find -type f -name $installerSchema | while read line
do
    if [ $? == 0 ]
    then
	packageSourceFolder="$(dirname "$(realpath $line)")"
	cd "$packageSourceFolder"
	
	find ! -name $installerSchema -type f | while read fileFromPackage
	do
	    sourceFile="$(realpath $fileFromPackage)"
	    targetFile="$HOME${fileFromPackage:1}"
	    locationTargetFile=$(dirname "$targetFile")

	    if [ ! -d $locationTargetFile ]
	    then
				if [ -e $locationTargetFile ]
				then
					rm -rf $locationTargetFile
				fi
				mkdir -p $locationTargetFile
	    fi
	    
	    ln -fs $sourceFile $targetFile
	done
    fi

    cd $packageFolder
done

ln -sf $(pwd)/.os/gnulinux/.bin/ $HOME

find $(pwd)/.os/gnulinux/.local/share/applications -type f | while read -r line
	do
		ln -sf "$line" ~/.local/share/applications/
	done

[ -d ~/.local/share/fonts ] || mkdir -p ~/.local/share/fonts
find $(pwd)/.fonts -type f | while read -r line
	do
		ln -sf "$line" ~/.local/share/fonts
	done

# Create Firefox directories

mkdir -p ~/.mozilla/firefox/default ~/.mozilla/firefox/s

# Restart daemons so configuration files are read again.

killall sxhkd ; eval "sxhkd &" && disown
killall dunst ; eval "dunst &" && disown

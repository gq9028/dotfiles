# Font

Use bitmap font as the default font.

```
static const char *fonts[] = {
	"Misc Fixed:matrix=0.65 0 0 0.65"
```

# Colors

Set black and white colors.

```
	[SchemeNorm] = { "#ffffff", "#000000" },
	[SchemeSel] = { "#000000", "#ffffff" },
```
